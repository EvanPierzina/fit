# README #

![fit icon 180.png](https://bitbucket.org/repo/8GpBzy/images/2524970517-fit%20icon%20180.png)

Fit is a prototype for a health and fitness tracking app for iOS. It was my first attempt at building an iOS app on my own and allowed me to better understand the requirements for a fully featured version. It was only developed for larger iPhone screens (6 plus, 6S plus, 7 plus) due to time constraints.

It supports adding custom exercises and routines. With these, users can start, resume, and complete workouts. In its prototypical state, it does not support logging workouts.

There is also some information for beginners looking to get into fitness.

## I am no longer working on this version. Please be advised that it may be unstable. ##
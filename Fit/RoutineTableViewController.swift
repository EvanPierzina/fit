//
//  RoutineTableViewController.swift
//  Fit
//
//  Created by Evan Pierzina on 3/8/16.
//  Copyright © 2016 Evan Pierzina. All rights reserved.
//

import UIKit

class RoutineTableViewController: UITableViewController {
    
    var routines = Routines()
    @IBOutlet var routineTable: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Load in the routines
        routines = loadRoutines()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        tableView.reloadData() //this is lazy
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return routines.count()
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "RoutineCell", for: indexPath)
        cell.textLabel?.text = routines.routines[indexPath.row].name
        cell.detailTextLabel?.text = "\(routines.routines[indexPath.row].days.count) days"
        cell.imageView?.image = UIImage(named: "strength13")
        
        return cell
    }

    // MARK: - Navigation

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "RoutineToDaysSegue" {
            if let destination = segue.destination as? RoutineViewController {
                if let row = self.tableView.indexPathForSelectedRow?.row {
                    destination.routine = routines.routines[row]
                    destination.routines = routines
                    destination.currentRoutine = row
                }
            }
        }
        //add button pressed
        else if segue.identifier == "newRoutine" {
                let nextViewController = (segue.destination as! UINavigationController).topViewController as! RoutineViewController
                nextViewController.navBarColor = navigationController!.navigationBar.barTintColor
                nextViewController.barItemColor = navigationController!.navigationBar.tintColor
                nextViewController.titleAttributes = navigationController!.navigationBar.titleTextAttributes as [String : AnyObject]?
        }
    }
    
    // MARK: - NSCoding
    
    func loadRoutines() -> Routines {
        if let routines = NSKeyedUnarchiver.unarchiveObject(withFile: Routines.ArchiveURL.path) as? Routines {
            return routines
        }
        else {
            //new
            return Routines()
        }
    }
    
    // MARK: - Actions
    @IBAction func unwindFromRoutineEditorAfterAdding(_ sender: UIStoryboardSegue) {
        routines = loadRoutines()
        if let theSender = sender.source as? RoutineViewController {
            let indexPath = IndexPath(item: theSender.currentRoutine, section: 0)
            tableView.insertRows(at: [indexPath], with: .bottom)
        }
    }
    
    @IBAction func unwindFromRoutineEditorAfterDeleting(_ sender: UIStoryboardSegue) {
        routines = loadRoutines()
        if let selectedIndexPath = tableView.indexPathForSelectedRow {
            tableView.deleteRows(at: [selectedIndexPath], with: .bottom)
        }
    }
}

//
//  ActiveRoutineViewController.swift
//  Fit
//
//  Created by Evan Pierzina on 4/4/16.
//  Copyright © 2016 Evan Pierzina. All rights reserved.
//

import UIKit

class ActiveRoutineViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    var tagExerciseArray = Array<Array<Int>>() //[button tag][exercise/table row that uses the tag]
    var firstTagOfExercise = Array<Int>() //[first button tag of exercise/table row]
    let tableView : UITableView = UITableView()
    var tagCounter : Int = 0
    var routine : Routine!
    var routineDay : Int = 0
    var currentWorkout : CurrentWorkout!
    
    //These will be set based on screen size
    var buttonHeight : CGFloat = 0
    var buttonMarginWidth: CGFloat = 0
    var buttonMarginHeight: CGFloat = 0
    var cellContentSubviewWidth : CGFloat = 0
    var cellContentSubviewMarginWidth : CGFloat = 0
    var cellContentSubviewMarginHeight : CGFloat = 0
    var titleSubviewHeight : CGFloat = 0
    let cornerRadius : CGFloat = 10
    let primaryColor = UIColor(colorLiteralRed: 161.0/255, green: 198.0/255, blue: 82.0/255, alpha: 1)
    let secondaryColor = UIColor(colorLiteralRed: 248.0/255, green: 248.0/255, blue: 248.0/255, alpha: 1)
    let tableViewBackgroundColor = UIColor(colorLiteralRed: 234.0/255, green: 234.0/255, blue: 234.0/255, alpha: 1)
    
    //Timer stuff
    let timerView = UIView()
    let timerLabel = UILabel()
    var timer = Timer()
    var currentSeconds = 0
    var maxSeconds = 0
    let timerBar = UIProgressView()
    let timerBarLabel = UILabel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "\(currentWorkout.routine.name)"
        
        view.addSubview(tableView)
        tableView.dataSource = self
        tableView.delegate = self
        tableView.register(ExerciseCell.self, forCellReuseIdentifier: "ExerciseCell")
        
        //tableView
        let tableViewHeight = view.frame.height - self.navigationController!.navigationBar.frame.height * 2 - UIApplication.shared.statusBarFrame.size.height
        let tableViewWidth = view.frame.width
        tableView.frame = CGRect(x: 0, y: 0, width: tableViewWidth, height: tableViewHeight)
        tableView.backgroundColor = Color.viewBackgroundColor
        tableView.separatorColor = UIColor.clear
        
        //bottom button container
        let bottomContainer = UIView()
        let bottomContainerHeight = self.navigationController!.navigationBar.frame.height
        bottomContainer.frame = CGRect(x: 0, y: tableViewHeight, width: tableViewWidth, height: bottomContainerHeight)
        bottomContainer.backgroundColor = Color.contentBackgroundColor
        var separator = CALayer()
        separator.frame = CGRect(x: 0, y: 0, width: tableViewWidth, height: 1)
        separator.backgroundColor = Color.greenColor.cgColor
        bottomContainer.layer.addSublayer(separator)
        view.addSubview(bottomContainer)
        
        //edit and finish buttons
        let editButton = UIButton()
        let finishButton = UIButton()
        editButton.frame = CGRect(x: 0, y: 0, width: tableViewWidth / 2 + 1, height: bottomContainerHeight)
        separator = CALayer()
        separator.frame = CGRect(x: tableViewWidth / 2, y: 0, width: 1, height: bottomContainerHeight)
        separator.backgroundColor = Color.greenColor.cgColor
        editButton.layer.addSublayer(separator)
        finishButton.frame = CGRect(x: tableViewWidth / 2 + 1, y: 0, width: tableViewWidth / 2 - 1, height: bottomContainerHeight)
        editButton.setTitle("Edit", for: UIControlState())
        finishButton.setTitle("Finish", for: UIControlState())
        editButton.setTitleColor(Color.greenColor, for: UIControlState())
        finishButton.setTitleColor(Color.greenColor, for: UIControlState())
        editButton.titleLabel?.font = UIFont(name: "HelveticaNeue", size: 20.0)
        finishButton.titleLabel?.font = UIFont(name: "HelveticaNeue", size: 20.0)
        editButton.addTarget(self, action: #selector(ActiveRoutineViewController.editButtonPressed), for: UIControlEvents.touchUpInside)
        finishButton.addTarget(self, action: #selector(ActiveRoutineViewController.finishButtonPressed), for: UIControlEvents.touchUpInside)
        bottomContainer.addSubview(editButton)
        bottomContainer.addSubview(finishButton)
        
        routine = currentWorkout.routine
        routineDay = currentWorkout.day
        
        //timerView
        timerView.isHidden = true
        timerView.frame = CGRect(x: 0, y: tableViewHeight - bottomContainerHeight, width: tableViewWidth, height: bottomContainerHeight)
        timerView.backgroundColor = UIColor(colorLiteralRed: 25.0/255, green: 25.0/255, blue: 25.0/255, alpha: 0.8)
        view.addSubview(timerView)
        timerLabel.textColor = Color.contentBackgroundColor
        timerLabel.frame = CGRect(x: timerView.frame.width * 0.75, y: 0, width: timerView.frame.width * 0.25, height: timerView.frame.height)
        timerLabel.center.y = timerView.frame.height / 2
        timerLabel.textAlignment = .center
        timerLabel.font = UIFont(name: "HelveticaNeue", size: 25.0)
        timerView.addSubview(timerLabel)
        
        let timerBarContainer = UIView()
        timerBarContainer.frame = CGRect(x: timerView.frame.width * 0.025, y: timerView.frame.height * 0.20, width: timerView.frame.width * 0.7, height: timerView.frame.height * 0.6)
        timerBarContainer.clipsToBounds = true
        timerBarContainer.layer.borderColor = Color.contentBackgroundColor.cgColor
        timerBarContainer.layer.borderWidth = 1
        timerBarContainer.layer.cornerRadius = cornerRadius
        timerView.addSubview(timerBarContainer)
        
        timerBar.frame = CGRect(x: 0, y: 0, width: timerBarContainer.frame.width, height: timerBarContainer.frame.height)
        timerBar.transform = timerBar.transform.scaledBy(x: 1, y: timerView.frame.height * 0.6 / timerBar.frame.height)
        timerBar.center.y = timerBarContainer.frame.height / 2
        timerBar.trackTintColor = UIColor.clear
        timerBar.progressTintColor = Color.greenColor
        timerBarContainer.addSubview(timerBar)
        
        timerBarLabel.frame = timerBar.frame
        timerBarLabel.textColor = Color.contentBackgroundColor
        timerBarLabel.font = UIFont(name: "HelveticaNeue", size: 17.0)
        timerBarLabel.textAlignment = .center
        timerBarContainer.addSubview(timerBarLabel)
        
        //set up variables based on screen size
        let screenWidth = view.frame.size.width
        cellContentSubviewWidth = screenWidth * 0.95 //2.5% margins on each side
        cellContentSubviewMarginWidth = screenWidth * 0.025
        
        /*
         5 buttons, 6 margins
         20% of the width should be used for margins
         80% of the width should be used for buttons
         */
        buttonHeight = cellContentSubviewWidth * 0.8 / 5    //square buttons; height = width
        buttonMarginWidth = cellContentSubviewWidth * 0.2 / 6
        
        let screenHeight = view.frame.size.height
        buttonMarginHeight = screenHeight * 0.01 //1% between buttons
        cellContentSubviewMarginHeight = screenHeight * 0.01 //1% between the cell's subview and the cell itself
        titleSubviewHeight = screenHeight * 0.05
    }
    
    func startTimer(_ seconds: Int, success: Bool) {
        timer.invalidate()
        currentSeconds = 0
        maxSeconds = seconds
        timerLabel.text = "0:00"
        timerBarLabel.text = success ? "Good job! Rest \(seconds / 60) minutes." : "Focus on completing your next set."
        timerBar.progress = 0.0
        timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(ActiveRoutineViewController.timerUpdate), userInfo: nil, repeats: true)
        timerView.isHidden = false
    }
    
    func timerUpdate() {
        currentSeconds += 1
        let minutes = (currentSeconds / 60) % 60
        let seconds = currentSeconds % 60
        if currentSeconds < 600 {
            timerLabel.text = String(format: "%01d:%02d", minutes, seconds)
        }
        else if currentSeconds < 3600 {
            timerLabel.text = String(format: "%02d:%02d", minutes, seconds)
        }
        else if currentSeconds <= 36000 {
            let hours = currentSeconds / 3600
            timerLabel.text = String(format: "%01d:%02d:%02d", hours, minutes, seconds)
        }
        else {
            let hours = currentSeconds / 3600
            timerLabel.text = String(format: "%02d:%02d:%02d", hours, minutes, seconds)
        }
        if currentSeconds < maxSeconds {
            timerBar.progress = (Float)(currentSeconds) / (Float)(maxSeconds)
        }
        else if currentSeconds == maxSeconds {
            timerBar.progress = 1.0
            timerBarLabel.text = "Go for it!"
            //play noise
        }
    }

    // MARK: - Table view data source

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return routine.days[routineDay].count
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if routine.days[routineDay][indexPath.row] is RoutineStrengthExercise {
            let rows = getRows(indexPath.row)
            return (buttonHeight + buttonMarginHeight) * rows + buttonMarginHeight + cellContentSubviewMarginHeight + titleSubviewHeight
        }
        return 40
    }
    
    func getRows(_ exerciseIndex: Int) -> CGFloat {
        if let exercise = routine.days[routineDay][exerciseIndex] as? RoutineStrengthExercise {
            var rows = CGFloat(exercise.sets.count / 5)
            if exercise.sets.count % 5 != 0 {
                rows += 1
            }
            return rows
        }
        return 0
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ExerciseCell", for: indexPath)
        
        cell.backgroundColor = UIColor.clear
        //create the subview that will contain all the content of the cell
        let cellContentSubview = UIView()
        cell.contentView.addSubview(cellContentSubview)
        cellContentSubview.layer.cornerRadius = cornerRadius
        cellContentSubview.layer.borderWidth = 1
        cellContentSubview.layer.borderColor = primaryColor.cgColor
        cellContentSubview.clipsToBounds = true
        cellContentSubview.frame = CGRect(x: cellContentSubviewMarginWidth, y: cellContentSubviewMarginHeight, width: cellContentSubviewWidth, height: cell.frame.height - cellContentSubviewMarginHeight)
        
        //titleSubview
        let titleSubview = UIView()
        titleSubview.frame = CGRect(x: 0, y: 0, width: cellContentSubviewWidth, height: titleSubviewHeight)
        titleSubview.backgroundColor = primaryColor
        cellContentSubview.addSubview(titleSubview)
        let title = UILabel()
        title.textColor = secondaryColor
        title.text = routine.days[routineDay][indexPath.row].name()
        title.frame = CGRect(x: buttonMarginWidth, y: 0, width: cellContentSubviewWidth / 2, height: titleSubviewHeight)
        title.center.y = titleSubviewHeight / 2
        titleSubview.addSubview(title)
        
        //also, set up each button to trigger a different event, then use their tags to identify the exercise row to enact behavior upon
        //also, also, disclosure flips and weightButton hides, revealing x out of y sets complete: "x/y sets"
        //may need to store weightButtons to unhide later
        
//        //titleSubviewButtonGroup
//        let titleSubviewButtonGroup = UIView()
//        titleSubviewButtonGroup.frame = CGRectMake(cellContentSubviewWidth * 0.66666, titleSubviewHeight * 0.1, cellContentSubviewWidth * 0.33333 - titleSubviewHeight * 0.1, titleSubviewHeight * 0.8)
//        titleSubviewButtonGroup.backgroundColor = secondaryColor
//        titleSubviewButtonGroup.layer.cornerRadius = cornerRadius
//        titleSubviewButtonGroup.layer.borderWidth = 1
//        titleSubviewButtonGroup.layer.borderColor = primaryColor.CGColor
//        titleSubviewButtonGroup.clipsToBounds = true
//        
//        ///////////////////////////////
//        //add label x/y sets here//////
//        ///////////////////////////////
//        
//        titleSubview.addSubview(titleSubviewButtonGroup)
//        
//        //disclosureButton
//        let disclosureButton = UIButton()
//        disclosureButton.frame = CGRectMake(titleSubviewButtonGroup.frame.width - titleSubviewButtonGroup.frame.height, 0, titleSubviewButtonGroup.frame.height, titleSubviewButtonGroup.frame.height)
//        disclosureButton.backgroundColor = secondaryColor
//        disclosureButton.layer.borderWidth = 1
//        disclosureButton.layer.borderColor = primaryColor.CGColor
//        disclosureButton.tag = indexPath.row
//        disclosureButton.addTarget(self, action: #selector(ActiveRoutineViewController.disclosureButtonPressed), forControlEvents: UIControlEvents.TouchUpInside)
//        titleSubviewButtonGroup.addSubview(disclosureButton)
        
        //weightButton
        if routine.days[routineDay][indexPath.row] is RoutineStrengthExercise {
            let weightButton = UIButton()
            let buttonMargin : CGFloat = titleSubviewHeight * 0.1
            weightButton.frame = CGRect(x: cellContentSubviewWidth * 0.75, y: buttonMargin, width: cellContentSubviewWidth * 0.25 - buttonMargin, height: titleSubviewHeight - buttonMargin * 2)
            weightButton.backgroundColor = secondaryColor
            weightButton.layer.cornerRadius = cornerRadius
            let weight = (routine.days[routineDay][indexPath.row] as! RoutineStrengthExercise).sets[0].weight
            //check if weight ends in 5 or 2.5 and display as int or float, respectivly
            if weight.truncatingRemainder(dividingBy: 1) < 0.5 {
                weightButton.setTitle("\((Int)(weight)) lbs", for: UIControlState())
            }
            else {
                weightButton.setTitle("\(weight) lbs", for: UIControlState())
            }
            weightButton.setTitleColor(primaryColor, for: UIControlState())
            weightButton.tag = indexPath.row //exercise index in routine
            weightButton.addTarget(self, action: #selector(ActiveRoutineViewController.weightButtonPressed), for: UIControlEvents.touchUpInside)
            titleSubview.addSubview(weightButton)
        }
        
        //setsSubview
        let setsSubview = UIView()
        setsSubview.frame = CGRect(x: 0, y: titleSubviewHeight, width: cellContentSubviewWidth, height: cellContentSubview.frame.height - titleSubviewHeight)
        setsSubview.backgroundColor = secondaryColor
        cellContentSubview.addSubview(setsSubview)
        
        if let exercise = routine.days[routineDay][indexPath.row] as? RoutineStrengthExercise {
            
            //calculate the warmup sets here based on the baseWeight
            
            //setsSubView
            var buttonCount = 0
            var x: CGFloat = buttonMarginWidth
            var y: CGFloat = buttonMarginHeight
            for set in exercise.sets {
                let button = UIButton()
                button.frame = CGRect(x: x, y: y, width: buttonHeight, height: buttonHeight)
                button.layer.cornerRadius = cornerRadius
                button.layer.borderWidth = 1
                button.layer.borderColor = primaryColor.cgColor
                if set.repsOrTimeCompleted == -1 {
                    button.setTitle("\(set.repsOrTime)", for: UIControlState())
                    button.setTitleColor(primaryColor, for: UIControlState())
//                    button.currentReps = -1
                }
                else {
                    button.setTitle("\(set.repsOrTimeCompleted)", for: UIControlState())
                    button.setTitleColor(secondaryColor, for: UIControlState())
                    button.backgroundColor = primaryColor
//                    button.currentReps = set.repsOrTimeCompleted
                }
                button.addTarget(self, action: #selector(ActiveRoutineViewController.setButtonPressed), for: UIControlEvents.touchUpInside)
                button.tag = tagCounter
//                button.maxReps = set.repsOrTime
                
                //check if this is the first exercise for this cell
                if firstTagOfExercise.count == indexPath.row {
                    firstTagOfExercise.append(tagCounter) //associates the exercise with its first set
                }
                
                tagExerciseArray.append([indexPath.row]) //associates the tag/index with its exercise/cell row
                
                setsSubview.addSubview(button)
                
                tagCounter += 1
                buttonCount += 1
                
                //set up x and y for next button
                if buttonCount % 5 == 0 {
                    //set up next x and y for a new row
                    y = y + buttonHeight + buttonMarginHeight
                    x = buttonMarginWidth
                }
                else {
                    //just shift x value
                    x = x + buttonHeight + buttonMarginWidth
                }
            }
        }
        
        return cell
    }
    
    // MARK: - Actions
    
    @IBAction func setButtonPressed(_ sender: UIButton) {
//        NSLog("tag: \(sender.tag)")
//        NSLog("exercise/row: \(tagExerciseArray[sender.tag][0])")
//        NSLog("button/set: \(sender.tag - firstTagOfExercise[tagExerciseArray[sender.tag][0]])")
        
        let exerciseIndex = tagExerciseArray[sender.tag][0]
        let setIndex = sender.tag - firstTagOfExercise[exerciseIndex]
        
        if let exercise = routine.days[routineDay][exerciseIndex] as? RoutineStrengthExercise {
            var sets = exercise.sets
            
            //go to the "non-tapped" state
            if sets[setIndex].repsOrTimeCompleted == 0 {
                sender.backgroundColor = secondaryColor
                sender.setTitleColor(primaryColor, for: UIControlState())
                sender.setTitle("\(sets[setIndex].repsOrTime)", for: UIControlState())
//                sender.currentReps = -1
                currentWorkout.activeButtons -= 1
                sets[setIndex].repsOrTimeCompleted = -1
                timerView.isHidden = true
                timer.invalidate()
            }
            //go to the "first tap" state
            else if sets[setIndex].repsOrTimeCompleted == -1 {
                sender.backgroundColor = primaryColor
                sender.setTitleColor(secondaryColor, for: UIControlState())
                sender.setTitle("\(sets[setIndex].repsOrTime)", for: UIControlState())
//                sender.currentReps = sender.maxReps
                sets[setIndex].repsOrTimeCompleted = sets[setIndex].repsOrTime
                currentWorkout.activeButtons += 1
                startTimer(180, success: true) //default for now
            }
            else {
//                sender.currentReps -= 1
                sets[setIndex].repsOrTimeCompleted -= 1
                sender.setTitle("\(sets[setIndex].repsOrTimeCompleted)", for: UIControlState())
                startTimer(300, success: false) //default for now
            }
            if currentWorkout.activeButtons > 0 {
                currentWorkout.isOngoing = true
            }
            else {
                currentWorkout.isOngoing = false
            }
            
            //dismiss timer if done with all sets
            var allSetsCompleted = true
            for set in sets {
                if set.repsOrTimeCompleted == -1 {
                    allSetsCompleted = false
                }
            }
            if allSetsCompleted {
                timerView.isHidden = true
                timer.invalidate()
            }
            
            saveCurrentWorkout()
        }
    }
    
    @IBAction func weightButtonPressed(_ sender: UIButton) {
        //show the weight edit menu for the exercise
        //sender.tag = indexPath.row
    }
    
//    @IBAction func disclosureButtonPressed(sender: UIButton) {
//        //flip the imageView of the chevron
//        //slide up the setsView for the exercise
//        //use the view hierarchy to get the setsView (sender.superview.subviews[0] is the syntax to use)
//    }
    
    @IBAction func editButtonPressed(_ sender: UIButton) {
        
    }
    
    @IBAction func finishButtonPressed(_ sender: UIButton) {
        //log it
        
        //update current max weight if needed, along with new weight if successful/3 fails in an exercise
        
        //reset currentWorkout (make a new current workout with the same one to reinitialize everything, and increment the day)
        let routines = NSKeyedUnarchiver.unarchiveObject(withFile: Routines.ArchiveURL.path) as! Routines
        var day = currentWorkout.day + 1
        if day >= routines.routines[(routines.names[currentWorkout.routine.name])!].days.count {
            day = 0
        }
        currentWorkout = CurrentWorkout(routine: routines.routines[(routines.names[currentWorkout.routine.name])!], day: day)
        NSKeyedArchiver.archiveRootObject(currentWorkout, toFile: CurrentWorkout.ArchiveURL.path)
        
        self.navigationController!.popViewController(animated: true)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    // MARK: - NSCoding
    
    func saveCurrentWorkout() {
        NSKeyedArchiver.archiveRootObject(currentWorkout, toFile: CurrentWorkout.ArchiveURL.path)
    }

}

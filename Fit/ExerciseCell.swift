//
//  ExerciseCell.swift
//  Fit
//
//  Created by Evan Pierzina on 4/19/16.
//  Copyright © 2016 Evan Pierzina. All rights reserved.
//

import UIKit

class ExerciseCell: UITableViewCell {
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.selectionStyle = .none
    }
}

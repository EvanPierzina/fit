//
//  Routines.swift
//  Fit
//
//  Created by Evan Pierzina on 4/16/16.
//  Copyright © 2016 Evan Pierzina. All rights reserved.
//

import Foundation

@objc class Routines : NSObject, NSCoding {
    
    var routines = [Routine]()
    var names = Dictionary<String, Int>()
    
    //this is used to initialize the object when there's nothing to unarchive
    override init() {
        super.init()
    }
    
    //the unarchiver will use this
    init(routines: [Routine], names: Dictionary<String, Int>) {
        self.routines = routines
        self.names = names
    }
    
    //Archive Paths
    static let DocumentsDirectory = FileManager().urls(for: .documentDirectory, in: .userDomainMask).first!
    static let ArchiveURL = DocumentsDirectory.appendingPathComponent("Routines")
    
    //NSCoding
    func encode(with aCoder: NSCoder) {
        aCoder.encode(routines, forKey: "routines")
        aCoder.encode(names, forKey: "names")
    }
    
    required convenience init?(coder aDecoder: NSCoder) {
        let routines = aDecoder.decodeObject(forKey: "routines") as! Array<Routine>
        let names = aDecoder.decodeObject(forKey: "names") as! Dictionary<String, Int>
        
        self.init(routines: routines, names: names)
    }
    
    func addRoutine(_ routine: Routine) {
//        let test:   String = routine.name
        names[routine.name] = routines.count //name -> index in routines
        routines.append(routine)
        //update self
        save()
    }
    
    func updateRoutine(_ routine: Routine) {
        routines[names[routine.name]!] = routine
        save()
    }
    
    func updateName(_ routine: Routine, oldName: String, newName: String) {
        names[newName] = names[oldName]
        names[oldName] = nil
        
//        names.setObject(names.objectForKey(oldName)!, forKey: newName)
//        names.removeObjectForKey(oldName)
        updateRoutine(routine)
    }
    
    func removeRoutine(_ routine: Routine) {
        routines.remove(at: names[routine.name]!)
        names[routine.name] = nil
//        names.removeObjectForKey(routine.name)
        save()
    }
    
    func save() {
        NSKeyedArchiver.archiveRootObject(self, toFile: Routines.ArchiveURL.path)
    }
    
    func count() -> Int {
        return routines.count
    }
}

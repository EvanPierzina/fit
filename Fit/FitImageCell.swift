//
//  FitImageCell.swift
//  Fit
//
//  Created by Evan Pierzina on 3/15/16.
//  Copyright © 2016 Evan Pierzina. All rights reserved.
//

import UIKit

class FitImageCell: UITableViewCell {
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.imageView!.frame = CGRect(x: 20,y: 5,width: 34,height: 34) //width for image = 74
    }
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
    }
}

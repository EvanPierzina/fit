//
//  ExerciseTableViewController.swift
//  Fit
//
//  Created by Evan Pierzina on 3/6/16.
//  Copyright © 2016 Evan Pierzina. All rights reserved.
//

import UIKit

class ExerciseTableViewController: UITableViewController {

    @IBOutlet weak var searchBar: UISearchBar!
    var searchIsActive: Bool = false
    
    var filteredStrengthResults: [StrengthExercise] = []
    var filteredCardioResults: [CardioExercise] = []
    var strengthExercises = [StrengthExercise]()
    var cardioExercises = [CardioExercise]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Load in the exercises if they exist
        if let savedStrengthExercises = loadStrengthExercises() {
            strengthExercises = savedStrengthExercises
        }
        if let savedCardioExercises = loadCardioExercises() {
            cardioExercises = savedCardioExercises
        }
        
        tableView.register(FitImageCell.self, forCellReuseIdentifier: "FitImageCell")
    }

    // MARK: - Table view data

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return searchIsActive ? section == 0 ? filteredStrengthResults.count : filteredCardioResults.count : section == 0 ? strengthExercises.count : cardioExercises.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ExerciseCell", for: indexPath)
        
        if indexPath.section == 0 {
            cell.textLabel?.text = searchIsActive ? filteredStrengthResults[indexPath.row].name : strengthExercises[indexPath.row].name
            
            switch strengthExercises[indexPath.row].muscleGroup {
            case StrengthExercise.upperArms:
                cell.imageView?.image = UIImage(named: "strength1")
                break
            case StrengthExercise.forearms:
                cell.imageView?.image = UIImage(named: "strength2")
                break
            case StrengthExercise.neck:
                cell.imageView?.image = UIImage(named: "strength3")
                break
            case StrengthExercise.shoulders:
                cell.imageView?.image = UIImage(named: "strength4")
                break
            case StrengthExercise.back:
                cell.imageView?.image = UIImage(named: "strength5")
                break
            case StrengthExercise.chest:
                cell.imageView?.image = UIImage(named: "strength6")
                break
            case StrengthExercise.waist:
                cell.imageView?.image = UIImage(named: "strength7")
                break
            case StrengthExercise.hips:
                cell.imageView?.image = UIImage(named: "strength8")
                break
            case StrengthExercise.thighs:
                cell.imageView?.image = UIImage(named: "strength9")
                break
            case StrengthExercise.calves:
                cell.imageView?.image = UIImage(named: "strength10")
                break
            default:
                break
            }
        }
        else {
            cell.textLabel?.text = searchIsActive ? filteredCardioResults[indexPath.row].name : cardioExercises[indexPath.row].name
            cell.imageView?.image = UIImage(named: "cardio13")

        }
        return cell
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        
        if section == 0 {
            return "Strength"
        }
        return "Cardio"
    }
    
    // A super ugly function to maintain the exercise list
    func insertExercise(_ exercise: Exercise) -> Int {
        if exercise is StrengthExercise {
            //place it in alphabetical order according to muscle group
            let targetGroup = (exercise as! StrengthExercise).muscleGroup
            let targetName = (exercise as! StrengthExercise).name
            
            var i: Int = 0
            for _ in strengthExercises {
                if targetGroup <= strengthExercises[i].muscleGroup {
                    //found the starting point for the appropriate muscle group
                    while (i < strengthExercises.count && strengthExercises[i].muscleGroup == targetGroup) {
                        //search through names
                        if strengthExercises[i].name < targetName {
                            i += 1
                        }
                        else {
                            //found the spot for it to go
                            strengthExercises.insert((exercise as! StrengthExercise), at: i)
                            return i
                        }
                    }
                    //either at the end of the array or the right spot to add
                    if i == strengthExercises.count {
                        //reached the end
                        strengthExercises.append(exercise as! StrengthExercise)
                        return i
                    }
                    else {
                        strengthExercises.insert((exercise as! StrengthExercise), at: i)
                        return i
                    }
                }
                else if i == strengthExercises.count - 1 {
                    //at the end of the array and haven't found a match
                    strengthExercises.append(exercise as! StrengthExercise)
                    return i + 1
                }
                i += 1
            }
            //nothing in the array, just add it
            strengthExercises.append(exercise as! StrengthExercise)
            return i
        }
        else {
            var i: Int = 0
            for _ in cardioExercises {
                if exercise.name < cardioExercises[i].name {
                    cardioExercises.insert(exercise as! CardioExercise, at: i)
                    return i
                }
                i += 1
            }
            cardioExercises.append(exercise as! CardioExercise)
            return i
        }
    }
    
    // MARK: - Search Bar
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        if searchBar.text != "" {
            searchIsActive = true
        }
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        searchIsActive = false
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchIsActive = false
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchIsActive = false
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchText == "" {
            searchIsActive = false
        }
        else {
            searchIsActive = true
            filteredStrengthResults = strengthExercises.filter {
                $0.name.range(of: searchText, options: .caseInsensitive) != nil
            }
            filteredCardioResults = cardioExercises.filter {
                $0.name.range(of: searchText, options: .caseInsensitive) != nil
            }
        }
        self.tableView.reloadData()
    }
    
    // MARK: - Navigation

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        //add button pressed
        if segue.identifier == "newExercise" {
            let nextViewController = (segue.destination as! UINavigationController).topViewController as! ExerciseViewController
            nextViewController.navBarColor = navigationController!.navigationBar.barTintColor
            nextViewController.barItemColor = navigationController!.navigationBar.tintColor
            nextViewController.titleAttributes = navigationController!.navigationBar.titleTextAttributes as [String : AnyObject]?
        }
        //cell was selected
        else {
            let nextViewController = segue.destination as! ExerciseViewController
            if let selectedExerciseCell = sender as? FitImageCell {
                let indexPath = tableView.indexPath(for: selectedExerciseCell)!
                
                if indexPath.section == 0 {
                    if searchIsActive {
                        nextViewController.strengthExercise = filteredStrengthResults[indexPath.row]
                    }
                    else {
                        nextViewController.strengthExercise = strengthExercises[indexPath.row]
                    }
                }
                else {
                    if searchIsActive {
                        nextViewController.cardioExercise = filteredCardioResults[indexPath.row]
                    }
                    else {
                        nextViewController.cardioExercise = cardioExercises[indexPath.row]
                    }
                }
            }
        }
    }
    
    @IBAction func unwindToExerciseTable(_ sender: UIStoryboardSegue) {
        if let sourceViewController = sender.source as? ExerciseViewController {
            
            //user returned from editing an exercise
            if let selectedIndexPath = tableView.indexPathForSelectedRow {
                if let exercise = sourceViewController.strengthExercise {
                    //check that it's in the same section
                    if selectedIndexPath.section != 0 {
                        //not in same section, remove previous and add this one
                        cardioExercises.remove(at: selectedIndexPath.row)
                        
                        //insert it
                        let newIndex = insertExercise(exercise)
                        let newIndexPath = IndexPath(item: newIndex, section: 0)
                        
                        tableView.moveRow(at: selectedIndexPath, to: newIndexPath)
                        tableView.reloadRows(at: [newIndexPath], with: .none)
                    }
                    //check if the name or muscle group changed
                    else if exercise.name != strengthExercises[selectedIndexPath.row].name || exercise.muscleGroup != strengthExercises[selectedIndexPath.row].muscleGroup {
                        //remove old one
                        strengthExercises.remove(at: selectedIndexPath.row)
                        
                        //insert new one
                        let newIndex = insertExercise(exercise)
                        let newIndexPath = IndexPath(item: newIndex, section: 0)
                        
                        tableView.moveRow(at: selectedIndexPath, to: newIndexPath)
                        tableView.reloadRows(at: [newIndexPath], with: .none)
                    }
                    saveStrengthExercises()
                }
                
                else if let exercise = sourceViewController.cardioExercise {
                    //check that it's in the same section
                    if selectedIndexPath.section != 1 {
                        //not in same section, remove previous and add this one
                        strengthExercises.remove(at: selectedIndexPath.row)
                        
                        //insert it
                        let newIndex = insertExercise(exercise)
                        let newIndexPath = IndexPath(item: newIndex, section: 1)
                        
                        tableView.moveRow(at: selectedIndexPath, to: newIndexPath)
                        tableView.reloadRows(at: [newIndexPath], with: .none)
                    }
                    //check if the name changed
                    else if exercise.name != strengthExercises[selectedIndexPath.row].name {
                        //remove old one
                        cardioExercises.remove(at: selectedIndexPath.row)
                        
                        //insert new one
                        let newIndex = insertExercise(exercise)
                        let newIndexPath = IndexPath(item: newIndex, section: 1)
                        
                        tableView.moveRow(at: selectedIndexPath, to: newIndexPath)
                        tableView.reloadRows(at: [newIndexPath], with: .none)
                    }
                    saveCardioExercises()
                }
                
            }
                
            else {
                // Add a new exercise
                if let exercise = sourceViewController.strengthExercise {
                    let newIndex = insertExercise(exercise)
                    let newIndexPath = IndexPath(item: newIndex, section: 0)
                    tableView.insertRows(at: [newIndexPath], with: .bottom)
                    
                    //save it
                    saveStrengthExercises()
                }
                else if let exercise = sourceViewController.cardioExercise {
                    let newIndex = insertExercise(exercise)
                    let newIndexPath = IndexPath(item: newIndex, section: 1)
                    tableView.insertRows(at: [newIndexPath], with: .bottom)
                    
                    //save it
                    saveCardioExercises()
                }
            }
        }
    }
    
    //user chose to delete the exercise
    @IBAction func removeExercise(_ sender: UIStoryboardSegue) {
        if let selectedIndexPath = tableView.indexPathForSelectedRow {
            if selectedIndexPath.section == 0 {
                strengthExercises.remove(at: selectedIndexPath.row)
                saveStrengthExercises()
            }
            else {
                cardioExercises.remove(at: selectedIndexPath.row)
                saveCardioExercises()
            }
            tableView.deleteRows(at: [selectedIndexPath], with: .bottom)
        }
    }
    
    // MARK: - NSCoding
    func saveStrengthExercises() {
        let isSuccessfulSave = NSKeyedArchiver.archiveRootObject(strengthExercises, toFile: StrengthExercise.ArchiveURL.path)
        if !isSuccessfulSave {
            print("Failed to save exercises...")
        }
    }
    
    func loadStrengthExercises() -> [StrengthExercise]? {
        return NSKeyedUnarchiver.unarchiveObject(withFile: StrengthExercise.ArchiveURL.path) as? [StrengthExercise]
    }
    
    func saveCardioExercises() {
        let isSuccessfulSave = NSKeyedArchiver.archiveRootObject(cardioExercises, toFile: CardioExercise.ArchiveURL.path)
        if !isSuccessfulSave {
            print("Failed to save exercises...")
        }
    }
    
    func loadCardioExercises() -> [CardioExercise]? {
        return NSKeyedUnarchiver.unarchiveObject(withFile: CardioExercise.ArchiveURL.path) as? [CardioExercise]
    }
}

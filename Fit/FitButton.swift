//
//  FitButton.swift
//  Fit
//
//  Created by Evan Pierzina on 3/4/16.
//  Copyright © 2016 Evan Pierzina. All rights reserved.
//

/*
This class is representative of all of the buttons used in the app.
It determines the styling aspects, but not the dimensions
*/
import UIKit

class FitButton: UIButton {

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        
        //This will be handy for changing themes
        
        //button ui
//        self.layer.backgroundColor = UIColor(colorLiteralRed: 224.0/255, green: 237.0/255, blue: 239.0/255, alpha: 1).CGColor
//        self.layer.borderWidth = 1
//        self.layer.borderColor = UIColor(colorLiteralRed: 0.85, green: 0.85, blue: 0.85, alpha: 0.5).CGColor
        self.layer.cornerRadius = 10
        self.clipsToBounds = true
    }

}
//
//  InfoViewController.swift
//  Fit
//
//  Created by Evan Pierzina on 3/4/16.
//  Copyright © 2016 Evan Pierzina. All rights reserved.
//

import UIKit

class InfoViewController: UIViewController {

    override func viewWillAppear(_ animated: Bool) {
        //set the title (it's a bit more complicated using this view hierarchy)
        self.tabBarController?.navigationItem.title = "Information"
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destination = segue.destination as? BeginnerInformationViewController {
            destination.infoIndex = (sender as AnyObject).tag //index of the button pressed
        }
    }

}

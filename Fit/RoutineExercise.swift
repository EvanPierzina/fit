//
//  RoutineExercise.swift
//  Fit
//
//  Created by Evan Pierzina on 3/8/16.
//  Copyright © 2016 Evan Pierzina. All rights reserved.
//

import UIKit

// A decorator for an Exercise that makes it applicable to a routine by adding sets and reps/time

@objc protocol RoutineExercise {
//    var exercise: Exercise { get set }
//    var sets: [Int] { get set }  //can contain variable amount of reps/time for each set
    
    func name() -> String;
    func image() -> UIImage?;
    func text() -> String?;
}
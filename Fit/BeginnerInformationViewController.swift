//
//  BeginnerInformationViewController.swift
//  Fit
//
//  Created by Evan Pierzina on 4/27/16.
//  Copyright © 2016 Evan Pierzina. All rights reserved.
//

import UIKit

class BeginnerInformationViewController: UITableViewController {

    var selectedRow:Int = -1
    var cellShouldExpand:Bool = false
    var infoIndex: Int!
    var information = Array<Array<String>>()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.separatorColor = UIColor.clear
        
        switch infoIndex {
        case 0:
            //beginner info
            self.title = "Beginner Info"
            information = [["General Disclaimer", "Anyone diagnosed with a medical condition should follow their doctor's advice on the level of activity that is right for them. The same goes for dieting, although you may want to consult a nutritionist instead of your general practitioner in this case. Do not disregard qualified medical advice on behalf of something you read here. Of course, the information provided by this app will be suitable for most people."],
                           ["Gender Differences", "With very few exceptions, all of the information found here applies to both genders. Keep in mind that most women will not gain muscle nearly at the same rate as men, so there isn't really a risk of becoming She-Hulk for women who choose to lift weights."],
                           ["Losing Weight", "For the most part, losing weight is dependent on your diet. Specifically, how many calories you take in and work off. Both running and lifting weights are great ways to lose weight because they burn off calories, but neither will matter if you are consuming too much. See the diet information section for more on what your daily goals should be to get you to your goal."],
                           ["Safety", "Always make sure you are using any equipment properly and treating it with respect. This is especially crucial with heavy weight-lifting. Do things properly; don't try to look cool. All it takes is one wrong move to seriously injure yourself, but the chance of injury is extremely reduced when using equipment the right way. If you don't know how something works or if you're unsure, ask someone else. Most people at the gym are nice and willing to help."],
                           ["Changing Your Mindset", "Being healthy is not something that you do for a few months and then stop. Whether you are trying to build muscle or lose weight, it is going to take a long time and dedication to changing yourself, but it will happen. When you get to the point you want to be at, be prepared to continue maintaining the healthy habits that got you there, spcifically your diet."],
                           ["You Can Reach Your Goals", "Save for individuals with rare medical conditions, the rest of this guide should provide you with enough information to guide you to where you want to get. Remember: changing your lifestyle does not happen overnight. You need to put in time and effort to achieve any goal, and becoming healther is no exception. If you really want to lose weight, gain muscle, or just improve your overall health, then it is up to you to make it happen!"]]
            break
        case 1:
            //Exercise Information
            self.title = "Exercise Info"
            information = [["Why Exercise?","While not as important as diet, exercise still plays a major role in your body's composition. Exercise is going to help you retain more muscle than you would otherwise, especially if you are lifting weights. While diet alone can get you to your target weight, remember that muscle is more dense than fat and so even at the same weight, having a higher composition of muscle will allow you to look more sleek overall compared to a composition with more weight from fat."],
                           ["Cardio Vs. Weights","For most people, a combination of both cardio and weight lifting is recommended. However, most people think that cardio is the best way to meet their goals, but this is often not the case. By lifting weights, you are also burning tons of calories, but in addition you are retaining more muscle and losing more fat. Cardio is fairly simple and recommended for keeping your heart healthy; however, it is not a substitute for weight lifting."],
                           ["The Benifits of Lifting Weights", "Lifting weights is recommended for everyone and has benefits for people of all ages, regardless of gender. There are many reasons to lift: it makes you stronger and healthier, it improves your posture and balance (making you less prone to injury, especially if you are older), and even speeds up your metabolism more than cardio does while resting."],
                           ["The Benefits of Cardio", "Cardio is also good for everyone. It builds endurance and allows you to exert yourself for longer periods of time, all while promoting cardiovascular health and even helping to strengthen bones. Additionally, it pretty much allows every body function better."],
                           ["What Should you be doing?", "It depends on your goals, but for most people who are looking to lose weight, the answer is almost always going to be both some cardio and some weights. If you are looking to gain weight, you may choose to favor lifting weights, but make sure to do some cardio every now and then. Even if you like the weight that you are at, both cardio and weights are still recommnded as they both have significant benefits for everyone."],
                           ["Best Beginner Exercises", "For cardio, the exercise doesn't matter too much. Just pick something and make sure to push yourself hard; go fast enough to at least break a sweat and start breathing hard.\n\nThe exercises that are going to benefit you the most are ones that involve compound movements. A compound movement means you are moving more than one joint, such as your elbows and shoulders during a bench press. This allows you to target more muscle groups with less exercises and, in general, the movements are closer to how you would use your body in real life situations. The opposite of a compound movement is an isolation movement, which can be good when you want to target one specific muscle or muscle group. However, isolation movements are not as necessary if you are interested in overall muscular development. Also, exercises that use machines are almost always inferior to their free weight equivalent, and although they may seem beginner-friendly, you will get better overall results from free weights. There are many beginner programs that come with the app that only involve compound movements, such as Starting Strength, so pick one and give it about a month and see how you like it. Beginner programs are designed to take you up to the point where you continually fail to complete exercises, also called a plateau, which may take a few months or even close to a year."],
                           ["Beginner Weight Lifting Tips", "#1: Don't be afraid of weights.\nSafety should always be a top priority when working out and if you are properly doing the exercise (meaning you have good form) then your chances of injury are going to be extemely small, especially when starting out. That being said, good form comes with experience, so it is very, very important that you start with small weights and work up while learning! When in doubt about form, ask or check out a YouTube video.\n\n#2: Warmups are important.\nIf you go to a gym, you will never see an experienced lifter just start doing an exercise at max weight. Take the time to warm up and work your way to lifting your target weight. An example of a proper warmup for squats will always start with just the bar for high reps and gradually get heavier with less reps until you get to your actual weight.\n\n#3: Keep a positive, focused attitude.\nLifting requires both physical strength and mental focus. Believe it or not, your thoughts will play a huge role in whether you succeed or fail when working out. In between sets, stay focused on completing your next lift. Of course, failure will happen if you are pushing yourself to your limit, but don't get upset; rather, embrace the fact that each day in the gym you are getting closer to your goal, be it stronger or to lose weight, regardless of whether you finished all your reps or not.\n\n#4: Don't give up.\nProgress can be slow, but you are making it happen with each trip to the gym or wherever you do your workout. Stick with it and get to your goal; don't make excuses, just make it happen!"],
                           ["Terminology","Repition (rep): doing an exercise properly 1 time.\n\nSet: a group of reps. For example, sets of 5 reps means you do an exercise 5 times for each set.\n\nBarbell: A long bar that you put weights on, meant for two-handed exercises.\n\nDumbbell: A small bar with weights on each end, meant for one-handed exercises."]]
            break
        case 2:
            //diet info
            self.title = "Diet Info"
            information = [["Losing and Gaining Weight", "The most basic fact of changing your body weight is simple: if you expend more calories than you consume, you will lose weight; alternatively, if you consume more calories than you expend, you will gain weight (be it fat, muscle or whatever). With that said, the exercise you do and what you consume has a significant impact on what you gain or lose in terms of fat and muscle. This idea of calorie intake also falls in line with research that suggests how often you eat and when do not make a difference, as long as you get your food for the day."],
                           ["General Advice", "If you are looking to become healthier, an obvious first step is to cut out the fast food, soda, and sweets. Fast foods are known to be heavily processed, which research has shown to have a negative impact, while (most) sodas and sweets are dense with calories that have no nutritional value. This doesn't mean that you can never have the things you like ever again, but rather that these things shouldn't be a part of every day. Challenge yourself to seek healthier alternatives, and especially try to drink more water; your body will thank you in time."],
                           ["Macronutrients","Carbohydrates, Proteins, and Fats. All three are necessary sources of energy.\n\nCarbohydrates (carbs) account for 4 calories per gram. They are not evil or bad for you in the appropriate amounts, but often people will consume too much. If any category, this should be the one to cut back on by consuming less things such as soda, candy, and white bread.\n\nProteins account for 4 calories per gram. Protein is known to help build muscle, lose fat, and support lean body mass. While protein itself is great, remember that you should be getting it (and most of your other macronutrients) from high quality sources. A general rule of thumb is to consume 1 gram of protein per pound of bodyweight each day.\n\nFats account for 9 calories per gram. Like carbs, they are also essential and not evil. Consuming fats does not mean you will get fat from it. The problem with fats is that there are different kinds in the category that people get too much of, namely saturated fats and trans fats, which can lead to high cholestorol. Some examples containing \"good\" fats include fish and flax seed oils."],
                           ["What You Should Eat", "Staples of a good diet should include, and are not limited to, things from the following categories:\n\nLean animal protein sources such as:\n Turkey and chicken in general\n Fish\n Eggs\n\nWhole grains:\n Oatmeal\n Whole wheat breads\n Whole wheat pasta\n Brown Rice\n\nFruits, vegetables, beans, and nuts\nHealthy fats, like olive oil\nDairy products"],
                           ["Counting Calories", "Depending on your goal, your daily calorie needs will vary. If you are looking to lose weight, you will want to aim for around 10 to 12 times your current bodyweight in calories each day. Conversely, if you are looking to gain muscle, you will want to go for around 16 to 18 times your current body weight. To maintain your current weight, the goal is 14 to 15 times your body weight.\n\nIt is strongly recommended that you count your calories via a tool such as MyFitnessPal (it's free and will take care of all the calculations for you), especially when starting out to get a handle on how much you are actually consuming."],
                           ["Do Not Starve To Lose Weight", "Starving your body is a bad way to lose weight. While it may seem like the best way to lose weight in the short-term, it's not maintainable, you will feel awful for doing it, and your diet will likely end in failure. Starving yourself also tends to cause more muscle loss than fat loss, which is the exact opposite of the goal here. The most you should be looking to lose is 1 pound per week; anything below that is not recommended."],
                           ["Additional Information for Women","Nutrition between genders is almost identical, with some key exceptions for women:\n\nMake sure you get enough iron. Women should be getting 50% more iron than men.\n\nMake sure you are getting enough calcium and vitamin D. Women, on average, are more prone to osteoprosis.\n\nFolic acid is recommended for women capable or planning to become pregnant to reduce birth defects."]]
            break
        case 3:
            //FAQ
            self.title = "Quesitons"
            information = [["How Do I Get Abs?", "Chances are, you already have abs and they just aren't visible. Visible abs require a low amount of body fat, and doing a bunch of situps isn't going to change that very much. If you want your abs to show, you need to get rid of the layer of fat on your stomach which is achieved only by losing weight from your body overall. You can't make one part of your body low on fat but not the others; the body doesn't work that way."],
                           ["Should I Get a Personal Trainer?","No.\n\nUnless you like gimmicky and ridiculuous exercises designed to give you a false sense that they are \"safer\" than traditional exercises. If you start with low weights doing a beginner program, you are not going to injure yourself and you also get to keep your money. If you think you need more help with exercises, check out some YouTube videos or Google it."]]
            break
        case 4:
            //Other Resources
            self.title = "Resources"
            
            break
        default:
            break
        }
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return information.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "BeginnerCell", for: indexPath) as! InfoTableViewCell
        
        cell.titleLabel.text = information[indexPath.row][0]
        cell.infoText.text = information[indexPath.row][1]
        cell.infoText.font = UIFont(name: "HelveticaNeue", size: 16.0)

        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if selectedRow != indexPath.row {
            //changed cell
            selectedRow = indexPath.row
            cellShouldExpand = true
            if let cell = tableView.cellForRow(at: indexPath) as? InfoTableViewCell {
                cell.infoText.flashScrollIndicators()
            }
        }
        else {
            cellShouldExpand = cellShouldExpand ? false : true
        }
        
        tableView.beginUpdates()
        tableView.endUpdates()
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == selectedRow {
            if cellShouldExpand == true {
                return 250.0
            }
            else {
                return 50.0
            }
        }
        return 50.0
    }

}

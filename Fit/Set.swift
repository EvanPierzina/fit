//
//  Set.swift
//  Fit
//
//  Created by Evan Pierzina on 4/12/16.
//  Copyright © 2016 Evan Pierzina. All rights reserved.
//

import Foundation

@objc class Set : NSObject, NSCoding {
    
    var repsOrTime: Int
    var weight: Float
    var repsOrTimeCompleted : Int = -1 //-1 means the set hasn't been started
    
    init(repsOrTime: Int, weight: Float) {
        self.repsOrTime = repsOrTime
        self.weight = weight
        
        super.init()
    }
    
    //used with NSCoding
    init(repsOrTime: Int, weight: Float, repsOrTimeCompleted: Int) {
        self.repsOrTime = repsOrTime
        self.weight = weight
        self.repsOrTimeCompleted = repsOrTimeCompleted
        
        super.init()
    }
    
    //Archive Paths
    static let DocumentsDirectory = FileManager().urls(for: .documentDirectory, in: .userDomainMask).first!
    static let ArchiveURL = DocumentsDirectory.appendingPathComponent("Set")
    
    //NSCoding
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(repsOrTime, forKey: "repsOrTime")
        aCoder.encode(weight, forKey: "weight")
        aCoder.encode(repsOrTimeCompleted, forKey: "repsOrTimeCompleted")
    }
    
    required convenience init?(coder aDecoder: NSCoder) {
        let repsOrTime = aDecoder.decodeInteger(forKey: "repsOrTime")
        let weight = aDecoder.decodeFloat(forKey: "weight")
        let repsOrTimeCompleted = aDecoder.decodeInteger(forKey: "repsOrTimeCompleted")
        
        self.init(repsOrTime: repsOrTime, weight: weight, repsOrTimeCompleted: repsOrTimeCompleted)
    }
}

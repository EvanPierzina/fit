//
//  RoutineExerciseViewController.swift
//  Fit
//
//  Created by Evan Pierzina on 3/12/16.
//  Copyright © 2016 Evan Pierzina. All rights reserved.
//

import UIKit

class RoutineExerciseViewController: UITableViewController {

    @IBOutlet weak var exerciseCell: UITableViewCell!
    @IBOutlet weak var saveButton: UIBarButtonItem!
    @IBOutlet weak var deleteButton: UIButton!

    let weightGranularity : Float = 5.0
    var currentExercise: Exercise?
    var routineExercise: RoutineExercise?
    
    @IBOutlet weak var repsView: UIView!
    @IBOutlet weak var repsSetsLabel: UILabel!
    @IBOutlet weak var repsLabel: UILabel!
    @IBOutlet weak var repsSetsSlider: UISlider!
    @IBOutlet weak var repsSlider: UISlider!
    @IBOutlet weak var repsProgressionLabel: UILabel!
    @IBOutlet weak var repsProgressionSlider: UISlider!
    @IBOutlet weak var repsWeight: UILabel!
    @IBOutlet weak var repsWeightSlider: UISlider!
    
    @IBOutlet weak var timedStrengthView: UIView!
    @IBOutlet weak var timedSetsLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var timedSetsSlider: UISlider!
    @IBOutlet weak var timeSlider: UISlider!
    @IBOutlet weak var timeUnitControl: UISegmentedControl!
    @IBOutlet weak var timedProgressionLabel: UILabel!
    @IBOutlet weak var timedProgressionSlider: UISlider!
    @IBOutlet weak var timedProgressionControl: UISegmentedControl!
    @IBOutlet weak var timedWeightLabel: UILabel!
    @IBOutlet weak var timedWeightSlider: UISlider!
    
    @IBOutlet weak var cardioView: UIView!
    @IBOutlet weak var cardioSetsLabel: UILabel!
    @IBOutlet weak var cardioSetsSlider: UISlider!
    @IBOutlet weak var cardioTimeLabel: UILabel!
    @IBOutlet weak var cardioTimeSlider: UISlider!
    @IBOutlet weak var cardioTypeControl: UISegmentedControl!
    @IBOutlet weak var cardioTimeUnitControl: UISegmentedControl!
    @IBOutlet weak var cardioTimeProgressionSlider: UISlider!
    @IBOutlet weak var cardioTimeProgressionUnitControl: UISegmentedControl!
    @IBOutlet weak var cardioTimeProgressionLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //initialize the view if needed
        if let _ = saveButton {
            setUI()
        }
    }
    
    func setUI() {
        if routineExercise != nil {
            saveButton.isEnabled = true
            deleteButton.isHidden = false
            self.title = routineExercise!.name()
            exerciseCell.textLabel?.text = routineExercise!.name()
            if let exercise = routineExercise as? RoutineStrengthExercise {
                currentExercise = exercise.exercise
                if exercise.isTimed() {
                    //timed view
                    timedStrengthView.isHidden = false
                    //set sliders, labels, and controls
                    timedSetsSlider.value = (Float)(exercise.sets.count)
                    timeSlider.value = (Float)(exercise.sets[0].repsOrTime) //going off of first value
                    timedProgressionSlider.value = (Float)(exercise.progression)
                    
                    timeUnitControl.selectedSegmentIndex = exercise.timeUnit!
                    timedProgressionControl.selectedSegmentIndex = exercise.progressionUnit!
                    
                    timedSetsLabel.text = "Sets: \((Int)(round(timedSetsSlider.value)))"
                    timeLabel.text = "Time: \((Int)(round(timeSlider.value))) \(timeUnitControl.selectedSegmentIndex == 0 ? ((round(timeSlider.value)) == 1.0 ? "Second" : "Seconds") : ((round(timeSlider.value)) == 1.0 ? "Minute" : "Minutes"))"
                    timedProgressionLabel.text = "Time added after successful day: \((Int)(round(timedProgressionSlider.value))) \(timedProgressionControl.selectedSegmentIndex == 0 ? ((round(timedProgressionSlider.value)) == 1.0 ? "Second" : "Seconds") : ((round(timedProgressionSlider.value)) == 1.0 ? "Minute" : "Minutes"))"
                }
                else {
                    //reps view
                    repsView.isHidden = false
                    //set sliders and labels
                    repsSetsSlider.value = (Float)(exercise.sets.count)
                    repsSlider.value = (Float)(exercise.sets[0].repsOrTime)
                    repsProgressionSlider.value = (Float)(exercise.progression)
                    repsSetsLabel.text = "Sets: \((Int)(round(repsSetsSlider.value)))"
                    repsLabel.text = "Reps: \((Int)(round(repsSlider.value)))"
                    repsProgressionLabel.text = "Weight added after successful day: \(exercise.progression) lbs"
                }
                updateWeightSliderIfNeeded()
            }
            else if let exercise = routineExercise as? RoutineCardioExercise {
                currentExercise = exercise.exercise
                //cardio view
                cardioView.isHidden = false
                //set sliders, controls, labels
                cardioSetsSlider.value = (Float)(exercise.sets.count)
                cardioTimeSlider.value = (Float)(exercise.sets[0])
                cardioTimeProgressionSlider.value = (Float)(exercise.progression)
                
                cardioTypeControl.selectedSegmentIndex = exercise.isTimed ? 0 : 1
                cardioTimeUnitControl.selectedSegmentIndex = exercise.timeUnit!
                cardioTimeProgressionUnitControl.selectedSegmentIndex = exercise.progressionTimeUnit!
                
                cardioSetsLabel.text = "Sets: \((Int)(round(cardioSetsSlider.value)))"
                cardioTimeLabel.text = "Time: \((Int)(round(cardioTimeSlider.value))) \(cardioTimeUnitControl.titleForSegment(at: cardioTimeUnitControl.selectedSegmentIndex)!))"
                cardioTimeProgressionLabel.text = "Time added after successful day: \((Int)(round(cardioTimeProgressionSlider.value))) \(cardioTimeProgressionUnitControl.titleForSegment(at: cardioTimeProgressionUnitControl.selectedSegmentIndex)!))"
            }
        }
        else {
            saveButton.isEnabled = false
            exerciseCell.textLabel?.text = "Choose an exercise"
            exerciseCell.textLabel?.textColor = UIColor.gray
        }
    }
    
    func updateWeightSliderIfNeeded() {
        if routineExercise != nil {
            if let exercise = routineExercise as? RoutineStrengthExercise {
                let slider = exercise.exercise.isTimed ? timedWeightSlider : repsWeightSlider
                let label = exercise.exercise.isTimed ? timedWeightLabel : repsWeight
                switch exercise.exercise.weightType {
                case StrengthExercise.bodyWeight:
                    slider?.minimumValue = -200 //why not
                    slider?.maximumValue = 300
                    slider?.value = 0.0
                    break
                case StrengthExercise.barbell:
                    slider?.minimumValue = 0
                    slider?.maximumValue = 800
                    slider?.value = 45
                    break
                case StrengthExercise.dumbbell:
                    slider?.minimumValue = 0
                    slider?.maximumValue = 300
                    slider?.value = 5
                    break
                case StrengthExercise.cable:
                    slider?.minimumValue = 0
                    slider?.maximumValue = 300
                    slider?.value = 20
                    break
                case StrengthExercise.kettlebell:
                    slider?.minimumValue = 0
                    slider?.maximumValue = 100
                    slider?.value = weightGranularity
                default:
                    break
                }
                slider?.value = exercise.sets[0].weight
                label?.text = "Weight: \(slider?.value) lbs"
            }

        }
        
        else if currentExercise != nil {
            if let exercise = currentExercise as? StrengthExercise {
                let slider = exercise.isTimed ? timedWeightSlider : repsWeightSlider
                let label = exercise.isTimed ? timedWeightLabel : repsWeight
                switch exercise.weightType {
                case StrengthExercise.bodyWeight:
                    slider?.minimumValue = -200 //why not
                    slider?.maximumValue = 300
                    slider?.value = 0.0
                    break
                case StrengthExercise.barbell:
                    slider?.minimumValue = 0
                    slider?.maximumValue = 800
                    slider?.value = 45
                    break
                case StrengthExercise.dumbbell:
                    slider?.minimumValue = 0
                    slider?.maximumValue = 300
                    slider?.value = 5
                    break
                case StrengthExercise.cable:
                    slider?.minimumValue = 0
                    slider?.maximumValue = 300
                    slider?.value = 20
                    break
                case StrengthExercise.kettlebell:
                    slider?.minimumValue = 0
                    slider?.maximumValue = 100
                    slider?.value = weightGranularity
                default:
                    break
                }
                label?.text = "Weight: \(slider?.value)"
            }
        }
    }
    
    func updateLabel(_ label: UILabel) {
        switch label {
        case repsSetsLabel:
            repsSetsLabel.text = "Sets: \((Int)(round(repsSetsSlider.value)))"
            break
        case repsLabel:
            repsLabel.text = "Reps: \((Int)(round(repsSlider.value)))"
            break
        case repsProgressionLabel:
            repsProgressionLabel.text = "Weight added after successful day: \(roundf(repsProgressionSlider.value / weightGranularity) * weightGranularity) lbs"
        case timedSetsLabel:
            timedSetsLabel.text = "Sets: \((Int)(round(timedSetsSlider.value)))"
            break
        case timeLabel:
            timeLabel.text = "Time: \((Int)(round(timeSlider.value))) \(timeUnitControl.selectedSegmentIndex == 0 ? ((round(timeSlider.value)) == 1.0 ? "Second" : "Seconds") : ((round(timeSlider.value)) == 1.0 ? "Minute" : "Minutes"))"
            break
        case timedProgressionLabel:
            timedProgressionLabel.text = "Time added after successful day: \((Int)(round(timedProgressionSlider.value))) \(timedProgressionControl.selectedSegmentIndex == 0 ? ((round(timedProgressionSlider.value)) == 1.0 ? "Second" : "Seconds") : ((round(timedProgressionSlider.value)) == 1.0 ? "Minute" : "Minutes"))"
            break
        case cardioSetsLabel:
            cardioSetsLabel.text = "Sets: \((Int)(round(cardioSetsSlider.value)))"
            break
        case cardioTimeLabel:
            cardioTimeLabel.text = "\(cardioTypeControl.selectedSegmentIndex == 0 ? "Time" : "Distance"): \((Int)(round(cardioTimeSlider.value))) \(cardioTypeControl.selectedSegmentIndex == 0 ? cardioTimeUnitControl.titleForSegment(at: cardioTimeUnitControl.selectedSegmentIndex)! : "Miles")"
            break
        case cardioTimeProgressionLabel:
            cardioTimeProgressionLabel.text = "\(cardioTypeControl.selectedSegmentIndex == 0 ? "Time" : "Distance") added after successful day: \((Int)(round(cardioTimeProgressionSlider.value))) \(cardioTypeControl.selectedSegmentIndex == 0 ? cardioTimeProgressionUnitControl.titleForSegment(at: cardioTimeProgressionUnitControl.selectedSegmentIndex)! : "Miles")"
            break
            //TODO: cardio time and progression sliders should allow finer granularity
        case repsWeight:
                repsWeight.text = "Weight: \(roundf(repsWeightSlider.value / weightGranularity) * weightGranularity)"
            break
        case timedWeightLabel:
            timedWeightLabel.text = "Weight: \(roundf(timedWeightSlider.value / weightGranularity) * weightGranularity)"
            break
        default:
            break
        }
    }
    
    //MARK: - Actions
    
    @IBAction func cancel(_ sender: AnyObject) {
        navigationController!.popViewController(animated: true)
    }
    
    @IBAction func sliderChangedValue(_ sender: UISlider) {
        switch sender {
        case repsSetsSlider:
            updateLabel(repsSetsLabel)
            break
        case repsSlider:
            updateLabel(repsLabel)
            break
        case repsProgressionSlider:
            updateLabel(repsProgressionLabel)
        case timedSetsSlider:
            updateLabel(timedSetsLabel)
            break
        case timeSlider:
            updateLabel(timeLabel)
            break
        case timedProgressionSlider:
            updateLabel(timedProgressionLabel)
            break
        case cardioSetsSlider:
            updateLabel(cardioSetsLabel)
            break
        case cardioTimeSlider:
            updateLabel(cardioTimeLabel)
            break
        case cardioTimeProgressionSlider:
            updateLabel(cardioTimeProgressionLabel)
            break
        case repsWeightSlider:
            updateLabel(repsWeight)
            break
        case timedWeightSlider:
            updateLabel(timedWeightLabel)
            break
        default:
            break
        }
    }
    
    @IBAction func segmentControlChangedValue(_ sender: UISegmentedControl) {
        switch sender {
        case cardioTypeControl:
            updateLabel(cardioTimeLabel)
            if cardioTypeControl.selectedSegmentIndex == 1 {
                cardioTimeUnitControl.isEnabled = false
                cardioTimeProgressionUnitControl.isEnabled = false
            }
            else {
                cardioTimeUnitControl.isEnabled = true
                cardioTimeProgressionUnitControl.isEnabled = true
            }
            break
        case cardioTimeProgressionUnitControl:
            updateLabel(cardioTimeProgressionLabel)
            break
        case cardioTimeUnitControl:
            updateLabel(cardioTimeLabel)
            break
        case timeUnitControl:
            updateLabel(timeLabel)
            break
        case timedProgressionControl:
            updateLabel(timedProgressionLabel)
            break
        default:
            break
        }
    }
    
    //changing exercise
    @IBAction func unwindFromRoutineExerciseTable(_ sender: UIStoryboardSegue) {
        if let sourceViewController = sender.source as? RoutineExerciseTableViewController {
            currentExercise = sourceViewController.selectedExercise
            
            //change the view with the name and type
            updateWeightSliderIfNeeded()
            saveButton.isEnabled = true
            exerciseCell.textLabel?.text = currentExercise?.name
            exerciseCell.textLabel?.textColor = UIColor.black
            self.title = currentExercise?.name
            if let exercise = sourceViewController.selectedExercise as? StrengthExercise {
                if exercise.isTimed {
                    //display timed strength view
                    repsView.isHidden = true
                    timedStrengthView.isHidden = false
                    cardioView.isHidden = true
                    updateLabel(timeLabel)
                    updateLabel(timedSetsLabel)
                    updateLabel(timedProgressionLabel)
                }
                else {
                    //display reps view
                    repsView.isHidden = false
                    timedStrengthView.isHidden = true
                    cardioView.isHidden = true
                    updateLabel(repsLabel)
                    updateLabel(repsSetsLabel)
                    updateLabel(repsProgressionLabel)
                }
            }
            else {
                //display the time/distance cardio view
                repsView.isHidden = true
                timedStrengthView.isHidden = true
                cardioView.isHidden = false
                updateLabel(cardioTimeLabel)
                updateLabel(cardioSetsLabel)
                updateLabel(cardioTimeProgressionLabel)
            }
        }
    }

    // MARK: - Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        //generate the RoutineExercise that will be passed back to either view
        if let exercise = currentExercise as? StrengthExercise {
            var sets = [Set]()
            if exercise.isTimed {
                for _ in 0...(Int)(round(timedSetsSlider.value) - 1) {
                    sets.append(Set(repsOrTime: (Int)(round(timeSlider.value)), weight: roundf(timedWeightSlider.value / weightGranularity) * weightGranularity))          //all sets same
                }
                routineExercise = RoutineStrengthExercise(exercise: exercise, sets: sets, progression: (round(timedProgressionSlider.value)), timeUnit: timeUnitControl.selectedSegmentIndex, progressionUnit: timedProgressionControl.selectedSegmentIndex, baseWeight: (Int)(round(timedWeightSlider.value)))
            }
            else {
                for _ in 0...(Int)(round(repsSetsSlider.value) - 1) {
                    sets.append(Set(repsOrTime: (Int)(round(repsSlider.value)), weight: roundf(repsWeightSlider.value / weightGranularity) * weightGranularity))         //all sets same
                }
                routineExercise = RoutineStrengthExercise(exercise: exercise, sets: sets, progression: roundf(repsProgressionSlider.value / weightGranularity) * weightGranularity, timeUnit: nil, progressionUnit: nil, baseWeight: (Int)(round(repsWeightSlider.value)))
            }
        }
        else if let exercise = currentExercise as? CardioExercise {
            var sets = [Int]()
            for _ in 0...(Int)(round(cardioSetsSlider.value) - 1) {
                sets.append((Int)(round(cardioTimeSlider.value)))        //all sets same
            }
            
            routineExercise = RoutineCardioExercise(exercise: exercise, sets: sets, isTimed: cardioTypeControl.selectedSegmentIndex == 0, timeUnit: cardioTimeUnitControl.selectedSegmentIndex, progression: (Int)(round(cardioTimeProgressionSlider.value)), progressionTimeUnit: cardioTimeProgressionUnitControl.selectedSegmentIndex)
        }
    }

}

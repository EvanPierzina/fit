//
//  RoutineViewController.swift
//  Fit
//
//  Created by Evan Pierzina on 3/11/16.
//  Copyright © 2016 Evan Pierzina. All rights reserved.
//

import UIKit

class RoutineViewController: UITableViewController, UITextFieldDelegate {
    
    @IBOutlet weak var days: UIStepper!
    @IBOutlet weak var daysLabel: UILabel!
    @IBOutlet weak var nameField: UITextField!
    @IBOutlet weak var deleteButton: UIButton!
    
    var currentRoutine = -1
    var routines = Routines()
    var routine: Routine?
    var tap: UITapGestureRecognizer?
    var isNewRoutine : Bool = false //true = not in the routines array yet
    var temporaryRoutine : Bool = false //true = in routines array, but not finalized
    var saveButton : UIBarButtonItem?
    
    //since this view may show modally, it must keep the UI theme consistent
    //via passing infomation through the segue
    var navBarColor : UIColor?
    var barItemColor : UIColor?
    var titleAttributes : [String : AnyObject]?

    override func viewDidLoad() {
        super.viewDidLoad()
        if routine != nil {
            //editing a routine
            deleteButton.isHidden = false
            self.title = routine!.name
            nameField.text = routine!.name
            days.value = (Double)(routine!.days.count)
            daysLabel.text = "Number of Days in Routine Cycle: \(routine!.days.count)"
            
        }
        else {
            //creating a routine
            isNewRoutine = true
            //have to load in routines array, if it exists
            if let routines = NSKeyedUnarchiver.unarchiveObject(withFile: Routines.ArchiveURL.path) as? Routines {
                self.routines = routines
                currentRoutine = routines.count() //this routine will be generated shortly...
            }
            
            //set up a cancel button
            self.navigationItem.leftBarButtonItem = UIBarButtonItem(barButtonSystemItem: .cancel, target: self, action: #selector(RoutineViewController.cancel))
            self.navigationController?.navigationBar.tintColor = UIColor.white
            
            //set up a save button
            saveButton = UIBarButtonItem(barButtonSystemItem: .save, target: self, action: #selector(RoutineViewController.saveNewRoutine))
            self.navigationItem.rightBarButtonItem = saveButton
            saveButton!.isEnabled = false //blank name = no saving
            
            //make a new routine with default settings and set it
            var days = [[RoutineExercise]]()
            days.append([RoutineExercise]()) //empty first day
            routine = Routine(name: "", days: days)
//            routines.addRoutine(routine!)
        }
        if navBarColor != nil {
            navigationController!.navigationBar.barTintColor = navBarColor
            navigationController!.navigationBar.tintColor = barItemColor
            navigationController!.navigationBar.titleTextAttributes = titleAttributes
        }
        
        tap = UITapGestureRecognizer(target: self, action: #selector(RoutineViewController.dismissKeyboard))
    }
    

//    func saveRoutines() {
//        routine?.name = nameField.text!
//        NSKeyedArchiver.archiveRootObject(routines, toFile: Routine.ArchiveURL.path!)
//    }
    
    // MARK: - TextField
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        view.removeGestureRecognizer(tap!)
        
        //validate name to enable the save button or discard change
        if isNewRoutine || temporaryRoutine {
            if textField.text != "" && routines.names[textField.text!] == nil {
                saveButton!.isEnabled = true
                
                routine!.name = textField.text!
                nameField.text = textField.text!
            }
            else {
                saveButton!.isEnabled = false
            }
        }
        //allow the edit if not "" AND the name doesn't exist yet (same name = do nothing)
        else if textField.text != "" && routines.names[textField.text!] == nil {
            let oldName = routine!.name
            routine!.name = textField.text!
            nameField.text = textField.text!
        
            routines.updateName(routine!, oldName: oldName, newName: routine!.name)
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        dismissKeyboard()
        return true
    }
    
    func dismissKeyboard() {
        //resigns first responder status for the UIView (aka hides the keyboard)
        view.endEditing(true)
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        view.addGestureRecognizer(tap!)
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        if routine != nil {
            return (Int)(days.value) //may need to round
        }
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return routine!.days[section].count + 1 //extra for adding an exercise
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "RoutineDayCell", for: indexPath)
        if routine != nil {
            
            if indexPath.row == routine!.days[indexPath.section].count {
                //extra row at the bottom of each day
                cell.textLabel?.text = "Add exercise"
                cell.textLabel?.textColor = UIColor(colorLiteralRed: 14.0/255, green: 179.0/255, blue: 204.0/255, alpha: 1)
                cell.imageView?.image = UIImage(named: "strength13")
            }
                
            else {
                cell.textLabel?.text = routine?.days[indexPath.section][indexPath.row].name()
                if let exercise = routine?.days[indexPath.section][indexPath.row] as? RoutineStrengthExercise {
                    switch exercise.exercise.muscleGroup {
                    case StrengthExercise.upperArms:
                        cell.imageView?.image = UIImage(named: "strength1")
                        break
                    case StrengthExercise.forearms:
                        cell.imageView?.image = UIImage(named: "strength2")
                        break
                    case StrengthExercise.neck:
                        cell.imageView?.image = UIImage(named: "strength3")
                        break
                    case StrengthExercise.shoulders:
                        cell.imageView?.image = UIImage(named: "strength4")
                        break
                    case StrengthExercise.back:
                        cell.imageView?.image = UIImage(named: "strength5")
                        break
                    case StrengthExercise.chest:
                        cell.imageView?.image = UIImage(named: "strength6")
                        break
                    case StrengthExercise.waist:
                        cell.imageView?.image = UIImage(named: "strength7")
                        break
                    case StrengthExercise.hips:
                        cell.imageView?.image = UIImage(named: "strength8")
                        break
                    case StrengthExercise.thighs:
                        cell.imageView?.image = UIImage(named: "strength9")
                        break
                    case StrengthExercise.calves:
                        cell.imageView?.image = UIImage(named: "strength10")
                        break
                    default:
                        break
                    }
                }
                else if routine?.days[indexPath.section][indexPath.row] is RoutineCardioExercise {
                    cell.imageView?.image = UIImage(named: "cardio13")
                }
            }
        }

        return cell
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return "Day \(section + 1)"
    }

    // MARK: - Navigation

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        //back item only exists for editing a routine
        if sender as AnyObject? === navigationController?.navigationBar.backItem {
            routines.updateRoutine(routine!)
        }
        
        if segue.identifier == "EditRoutineExercise" {
            if routine != nil {
                //pass the RoutineExercise
                if let destination = segue.destination as? RoutineExerciseViewController {
                    let indexPath = tableView.indexPathForSelectedRow!
                    //make sure it's not a "Add new exercise" cell
                    if indexPath.row != routine!.days[indexPath.section].count {
                        destination.routineExercise = routine!.days[indexPath.section][indexPath.row]
                    }
//                    else {
//                        //jump two views ahead, but push them both on the stack
//                        let optionsView = self.storyboard?.instantiateViewControllerWithIdentifier("RoutineExerciseViewController") as! RoutineExerciseViewController
//                        let exerciseTableView = self.storyboard?.instantiateViewControllerWithIdentifier("RoutineExerciseTableViewController") as! RoutineExerciseTableViewController
//                        
//                        if let navigationController = navigationController {
//                            
//                            navigationController.pushViewController(exerciseTableView, animated: true)
//                            
//                            let stackCount = navigationController.viewControllers.count
//                            let addIndex = stackCount - 1
//                            navigationController.viewControllers.insert(optionsView, atIndex: addIndex)
//                        }
//                    }
                }
            }
        }
    }
    
    // MARK: - Actions
    
    //user chose to save a routine exercise and came back to this view
    @IBAction func unwindToDayTableFromEdit(_ sender: UIStoryboardSegue) {
        if let sourceViewController = sender.source as? RoutineExerciseViewController {
            if let selectedIndexPath = tableView.indexPathForSelectedRow {
                if selectedIndexPath.row != routine!.days[selectedIndexPath.section].count {
                    //update the exercise
                    routine!.days[selectedIndexPath.section][selectedIndexPath.row] = sourceViewController.routineExercise!
                    //place the modified routine into the array of routines and save it
                    if isNewRoutine {
                        temporaryRoutine = true
                        isNewRoutine = false
                        routines.addRoutine(routine!)
                    }
                    else {
                        routines.updateRoutine(routine!)
                    }
//                    routines[currentRoutine] = routine!
//                    saveRoutines()
                    //reload
                    tableView.reloadRows(at: [selectedIndexPath], with: .none)
                }
                else {
                    //add the exercise
                    routine!.days[selectedIndexPath.section].append(sourceViewController.routineExercise!)
                    //place the modified routine into the array of routines and save it
                    if isNewRoutine {
                        temporaryRoutine = true
                        isNewRoutine = false
                        routines.addRoutine(routine!)
                    }
                    else {
                        routines.updateRoutine(routine!)
                    }
//                    routines[currentRoutine] = routine!
//                    saveRoutines()
                    //reload
                    tableView.insertRows(at: [selectedIndexPath], with: .bottom)
                }
                
            }
        }
    }
    
    //user chose to delete an exercise
    @IBAction func removeRoutineExercise(_ sender: UIStoryboardSegue) {
        if let selectedIndexPath = tableView.indexPathForSelectedRow {
            //remove
            routine!.days[selectedIndexPath.section].remove(at: selectedIndexPath.row)
            //save
            routines.updateRoutine(routine!)
//            routines[currentRoutine] = routine!
//            saveRoutines()
            //update
            tableView.deleteRows(at: [selectedIndexPath], with: .bottom)
        }
    }
    
    //stepper
    @IBAction func daysChanged(_ sender: UIStepper) {
        daysLabel.text = "Number of Days in Routine Cycle: \((Int)(days.value))"
        //see if a day is being added
        if (Int)(days.value) > routine!.days.count {
            //set up a new day or routines and add it
            routine?.days.append([RoutineExercise]())
        }
        else {
            //delete the day of routines
            routine?.days.remove(at: (Int)(days.value))
            //TODO: warn users if they're about to delete a day with exercises in them
        }
        //routines array only exists for editing
        if !routines.routines.isEmpty {
            if !isNewRoutine {
                routines.updateRoutine(routine!)
            }
            else {
                routines.addRoutine(routine!)
                isNewRoutine = false
                temporaryRoutine = true
            }
//            routines[currentRoutine] = routine!
//            saveRoutines()
        }
        tableView.reloadData()
    }
    
    //saving a new routine
    @IBAction func saveNewRoutine() {
        routine!.name = nameField.text!
        if temporaryRoutine {
            //store the temporary information along with anything new
            routines.updateRoutine(routine!)
        }
        else {
            routines.addRoutine(routine!)
        }
        self.performSegue(withIdentifier: "unwindFromRoutineEditorAfterAdding", sender: self)
    }
    
    @IBAction func deleteRoutine(_ sender: UIButton) {
        routines.removeRoutine(routine!)
//        routines.removeAtIndex(currentRoutine)
//        saveRoutines()
        self.performSegue(withIdentifier: "unwindFromRoutineEditorAfterDeleting", sender: self)
    }
    
    //cancel button pressed (only available when adding a new routine)
    @IBAction func cancel() {
        //remove the temporary routine if it was added
        if temporaryRoutine {
            routines.removeRoutine(routine!)
        }
//        routines.removeAtIndex(currentRoutine)
//        saveRoutines()
        dismiss(animated: true, completion: nil)
    }
}

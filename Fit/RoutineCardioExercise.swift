//
//  RoutineCardioExercise.swift
//  Fit
//
//  Created by Evan Pierzina on 3/8/16.
//  Copyright © 2016 Evan Pierzina. All rights reserved.
//

import UIKit

class RoutineCardioExercise : NSObject, RoutineExercise {
    
    var exercise: CardioExercise
    @objc var sets: [Int]           //can contain variable amount of time/distance for each set
    var isTimed: Bool               //timed or distance
    var timeUnit : Int?             //seconds/minutes/hours
    var progression: Int            //increased time or distance, depending on the exercise type
    var progressionTimeUnit: Int?    //seconds/minutes/hours
    
    static let seconds = 0
    static let minutes = 1
    static let hours = 2
    
    init(exercise: CardioExercise, sets: [Int], isTimed: Bool, timeUnit: Int?, progression: Int, progressionTimeUnit: Int?) {
        self.exercise = exercise
        self.sets = sets
        self.isTimed = isTimed
        self.timeUnit = timeUnit
        self.progression = progression
        self.progressionTimeUnit = progressionTimeUnit
    }
    
    @objc func name() -> String {
        return exercise.name
    }
    
    
    @objc func image() -> UIImage? {
        return exercise.image
    }
    
    @objc func text() -> String? {
        return exercise.text
    }
    
    //Archive Paths
    static let DocumentsDirectory = FileManager().urls(for: .documentDirectory, in: .userDomainMask).first!
    static let ArchiveURL = DocumentsDirectory.appendingPathComponent("RoutineCardioExercise")
    
    //NSCoding
    
    func encodeWithCoder(_ aCoder: NSCoder) {
        aCoder.encode(exercise, forKey: "exercise")
        aCoder.encode(sets, forKey: "sets")
        aCoder.encode(isTimed, forKey: "isTimed")
        aCoder.encode(timeUnit, forKey: "timeUnit")
        aCoder.encode(progression, forKey: "progression")
        aCoder.encode(progressionTimeUnit, forKey: "progressionTimeUnit")
    }
    
    required convenience init?(coder aDecoder: NSCoder) {
        
        let exercise = aDecoder.decodeObject(forKey: "exercise") as! CardioExercise
        let sets = aDecoder.decodeObject(forKey: "sets") as! [Int]
        let isTimed = aDecoder.decodeBool(forKey: "isTimed")
        let timeUnit = aDecoder.decodeObject(forKey: "timeUnit") as? Int
        let progression = aDecoder.decodeInteger(forKey: "progression")
        let progressionTimeUnit = aDecoder.decodeObject(forKey: "progressionTimeUnit") as? Int
        
        self.init(exercise: exercise, sets: sets, isTimed: isTimed, timeUnit: timeUnit, progression: progression, progressionTimeUnit: progressionTimeUnit)
    }

}

//
//  RoutineExercise.swift
//  Fit
//
//  Created by Evan Pierzina on 3/8/16.
//  Copyright © 2016 Evan Pierzina. All rights reserved.
//

import UIKit

// A decorator for an Exercise that makes it applicable to a routine by adding sets and reps/time

class RoutineStrengthExercise : NSObject, RoutineExercise {
    
    var exercise: StrengthExercise
    var sets: [Set]     //can contain variable amount of reps/time for each set
    var progression: Float
    var progressionUnit: Int? //seconds/minutes/hours
    var timeUnit: Int?
    var baseWeight: Int
    
    static let seconds = 0
    static let minutes = 1
    static let hours = 2
    
    init(exercise: StrengthExercise, sets: [Set], progression: Float, timeUnit: Int?, progressionUnit: Int?, baseWeight: Int) {
        self.exercise = exercise
        self.sets = sets
        self.progression = progression
        self.timeUnit = timeUnit
        self.progressionUnit = progressionUnit
        self.baseWeight = baseWeight
    }
    
    @objc func name() -> String {
        return exercise.name
    }
    
    func isTimed() -> Bool {
        return exercise.isTimed
    }
    
    @objc func image() -> UIImage? {
        return exercise.image
    }
    
    @objc func text() -> String? {
        return exercise.text
    }
    
    func weightType() -> Int {
        return exercise.weightType
    }
    
    func muscleGroup() -> Int {
        return exercise.muscleGroup
    }
    
    //Archive Paths
    static let DocumentsDirectory = FileManager().urls(for: .documentDirectory, in: .userDomainMask).first!
    static let ArchiveURL = DocumentsDirectory.appendingPathComponent("RoutineStrengthExercise")
    
    //NSCoding
    
    func encodeWithCoder(_ aCoder: NSCoder) {
        aCoder.encode(exercise, forKey: "exercise")
        aCoder.encode(sets, forKey: "sets")
        aCoder.encode(progression, forKey: "progression")
        aCoder.encode(timeUnit, forKey: "timeUnit")
        aCoder.encode(progressionUnit, forKey: "progressionUnit")
        aCoder.encode(baseWeight, forKey: "baseWeight")
    }
    
    required convenience init?(coder aDecoder: NSCoder) {
        
        let exercise = aDecoder.decodeObject(forKey: "exercise") as! StrengthExercise
        let sets = aDecoder.decodeObject(forKey: "sets") as! [Set]
        let progression = aDecoder.decodeFloat(forKey: "progression")
        let timeUnit = aDecoder.decodeObject(forKey: "timeUnit") as? Int
        let progressionUnit = aDecoder.decodeObject(forKey: "progressionUnit") as? Int
        let baseWeight = aDecoder.decodeInteger(forKey: "baseWeight")
        
        self.init(exercise: exercise, sets: sets, progression: progression, timeUnit: timeUnit, progressionUnit: progressionUnit, baseWeight: baseWeight)
    }
}

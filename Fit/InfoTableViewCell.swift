//
//  InfoTableViewCell.swift
//  Fit
//
//  Created by Evan Pierzina on 4/27/16.
//  Copyright © 2016 Evan Pierzina. All rights reserved.
//

import UIKit

class InfoTableViewCell: UITableViewCell {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var infoText: UITextView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        let sublayer = CALayer()
        sublayer.frame = CGRect(x: 0, y: titleLabel.frame.height - 1, width: titleLabel.frame.width, height: 1)
        sublayer.backgroundColor = Color.viewBackgroundColor.cgColor
        titleLabel.layer.addSublayer(sublayer)
    }

}

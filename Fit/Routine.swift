//
//  Routine.swift
//  Fit
//
//  Created by Evan Pierzina on 3/5/16.
//  Copyright © 2016 Evan Pierzina. All rights reserved.
//

import UIKit

// A decorator for an Exercise that makes it applicable to a routine by adding sets and reps/time

@objc class Routine : NSObject, NSCoding {
    
    var name: String
    var days: [[RoutineExercise]]
    
    init(name: String, days: [[RoutineExercise]]) {
        self.name = name
        self.days = days
        
        super.init()
    }
    
    //Archive Paths
    static let DocumentsDirectory = FileManager().urls(for: .documentDirectory, in: .userDomainMask).first!
    static let ArchiveURL = DocumentsDirectory.appendingPathComponent("Routines")

    //NSCoding

    func encode(with aCoder: NSCoder) {
        aCoder.encode(name, forKey: "name")
        aCoder.encode(days, forKey: "days")
    }

    required convenience init?(coder aDecoder: NSCoder) {
        let name = aDecoder.decodeObject(forKey: "name") as! String
        let days = aDecoder.decodeObject(forKey: "days") as! [[RoutineExercise]]
        
        self.init(name: name, days: days)
    }
}

//
//  WorkoutsViewController.swift
//  Fit
//
//  Created by Evan Pierzina on 3/4/16.
//  Copyright © 2016 Evan Pierzina. All rights reserved.
//

/*
This is the starting screen for the app.
It deals with workouts and first-time setup.
*/

import UIKit

class WorkoutsViewController: UIViewController, UIPickerViewDataSource, UIPickerViewDelegate {
    @IBOutlet weak var getStartedButton: UIButton!
    @IBOutlet weak var editWorkoutsButton: UIButton!
    @IBOutlet weak var routinePicker: UIPickerView!
    @IBOutlet weak var getStartedView: UIView!
    
    @IBOutlet var mainView: UIView!
    var routines = Routines()
    var strengthExercises = [StrengthExercise]()
    var cardioExercises = [CardioExercise]()
    var currentWorkout : CurrentWorkout!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = Color.contentBackgroundColor
        
        //pickerView stuff
        routinePicker.backgroundColor = Color.contentBackgroundColor
        getStartedView.layer.borderColor = Color.greenColor.cgColor
        getStartedView.backgroundColor = Color.greenColor
        getStartedView.layer.borderWidth = 1
        getStartedView.layer.cornerRadius = 10
        getStartedView.clipsToBounds = true
        
        if let savedStrengthExercises = loadStrengthExercises() {
            strengthExercises = savedStrengthExercises
        }
        else {
            loadDefaultStrengthExercises()
        }
        
        if let savedCardioExercises = loadCardioExercises() {
            cardioExercises = savedCardioExercises
        }
        
        //since the view controller relies on this data, it must be updated upon appearing
        if let routines = loadRoutines() {
            // Load in the exercises and routines if they exist
            self.routines = routines
        }
        else {
            //Load the default exercises if nothing is stored
            loadDefaultRoutines()
        }
        //this is needed on the load for the pickerView to populate
        if let currentWorkout = loadCurrentWorkout() {
            self.currentWorkout = currentWorkout
        }
        else {
            currentWorkout = CurrentWorkout(routine: routines.routines[0], day: 0)
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        //set the title (can't do this via interface builder due to view hierarchy)
        self.tabBarController?.navigationItem.title = "Home"
        
        if let currentWorkout = loadCurrentWorkout() {
            self.currentWorkout = currentWorkout
            getStartedButton.setTitle(currentWorkout.isOngoing ? "Continue Workout" : "Start Workout", for: UIControlState())
        }
        else {
            currentWorkout = CurrentWorkout(routine: routines.routines[0], day: 0)
            getStartedButton.setTitle("Start Workout", for: UIControlState())
        }
        
        if let routines = loadRoutines() {
            // Load in the exercises and routines if they exist
            self.routines = routines
        }
        else {
            //Load the default exercises if nothing is stored
            loadDefaultRoutines()
        }
        
        //update the picker in case something was modified
        routinePicker.reloadAllComponents()
        
        //attempt to find the current workout
        let index = routines.names[currentWorkout.routine.name]
        if index != nil {
            routinePicker.selectRow(index!, inComponent: 0, animated: false)
            //make sure the day is still in bounds
            if currentWorkout.day < routines.routines[index!].days.count {
                routinePicker.selectRow(currentWorkout.day, inComponent: 1, animated: false)
            }
        }
        
    }
    
    func loadDefaultRoutines() {
        //Starting Strength
        let exercise1 = StrengthExercise(name: "Back Squat", text: "Do it!", image: nil, muscleGroup: StrengthExercise.thighs, weightType: StrengthExercise.barbell, isTimed: false)
        let exercise2 = StrengthExercise(name: "Overhead Press", text: "Do it!", image: nil, muscleGroup: StrengthExercise.shoulders, weightType: StrengthExercise.barbell, isTimed: false)
        let exercise3 = StrengthExercise(name: "Deadlift", text: "Do it!", image: nil, muscleGroup: StrengthExercise.thighs, weightType: StrengthExercise.barbell, isTimed: false)
        let exercise4 = StrengthExercise(name: "Power Clean", text: "Do it!", image: nil, muscleGroup: StrengthExercise.hips, weightType: StrengthExercise.barbell, isTimed: false)
        let exercise5 = StrengthExercise(name: "Bench Press", text: "Do it!", image: nil, muscleGroup: StrengthExercise.chest, weightType: StrengthExercise.barbell, isTimed: false)
        
        let routineExercise1 = RoutineStrengthExercise(exercise: exercise1, sets: [Set(repsOrTime: 5, weight: 45), Set(repsOrTime: 5, weight: 45), Set(repsOrTime: 5, weight: 45)], progression: 5, timeUnit: nil, progressionUnit: nil, baseWeight: 45)
        let routineExercise2 = RoutineStrengthExercise(exercise: exercise2, sets: [Set(repsOrTime: 5, weight: 45), Set(repsOrTime: 5, weight: 45), Set(repsOrTime: 5, weight: 45)], progression: 5, timeUnit: nil, progressionUnit: nil, baseWeight: 45)
        let routineExercise3 = RoutineStrengthExercise(exercise: exercise3, sets: [Set(repsOrTime: 5, weight: 55)], progression: 5, timeUnit: nil, progressionUnit: nil, baseWeight: 55)
        let routineExercise4 = RoutineStrengthExercise(exercise: exercise4, sets: [Set(repsOrTime: 5, weight: 45), Set(repsOrTime: 5, weight: 45), Set(repsOrTime: 5, weight: 45)], progression: 5, timeUnit: nil, progressionUnit: nil, baseWeight: 45)
        let routineExercise5 = RoutineStrengthExercise(exercise: exercise5, sets: [Set(repsOrTime: 5, weight: 45), Set(repsOrTime: 5, weight: 45), Set(repsOrTime: 5, weight: 45)], progression: 5, timeUnit: nil, progressionUnit: nil, baseWeight: 45)
        
        let routine = Routine(name: "Starting Strength", days: [[routineExercise1, routineExercise2, routineExercise3], [routineExercise1, routineExercise4, routineExercise5]])
        routines.addRoutine(routine)
        
        //StrongLifts
        let exercise6 = StrengthExercise(name: "Back Squat", text: "Do it!", image: nil, muscleGroup: StrengthExercise.thighs, weightType: StrengthExercise.barbell, isTimed: false)
        let exercise7 = StrengthExercise(name: "Overhead Press", text: "Do it!", image: nil, muscleGroup: StrengthExercise.shoulders, weightType: StrengthExercise.barbell, isTimed: false)
        let exercise8 = StrengthExercise(name: "Deadlift", text: "Do it!", image: nil, muscleGroup: StrengthExercise.thighs, weightType: StrengthExercise.barbell, isTimed: false)
        let exercise9 = StrengthExercise(name: "Pendlay Rows", text: "Do it!", image: nil, muscleGroup: StrengthExercise.back, weightType: StrengthExercise.barbell, isTimed: false)
        let exercise10 = StrengthExercise(name: "Bench Press", text: "Do it!", image: nil, muscleGroup: StrengthExercise.chest, weightType: StrengthExercise.barbell, isTimed: false)
        
        let routineExercise6 = RoutineStrengthExercise(exercise: exercise6, sets: [Set(repsOrTime: 5, weight: 45), Set(repsOrTime: 5, weight: 45), Set(repsOrTime: 5, weight: 45)], progression: 5, timeUnit: nil, progressionUnit: nil, baseWeight: 45)
        let routineExercise7 = RoutineStrengthExercise(exercise: exercise7, sets: [Set(repsOrTime: 5, weight: 45), Set(repsOrTime: 5, weight: 45), Set(repsOrTime: 5, weight: 45)], progression: 5, timeUnit: nil, progressionUnit: nil, baseWeight: 45)
        let routineExercise8 = RoutineStrengthExercise(exercise: exercise8, sets: [Set(repsOrTime: 5, weight: 55)], progression: 5, timeUnit: nil, progressionUnit: nil, baseWeight: 55)
        let routineExercise9 = RoutineStrengthExercise(exercise: exercise9, sets: [Set(repsOrTime: 5, weight: 55), Set(repsOrTime: 5, weight: 55), Set(repsOrTime: 5, weight: 55)], progression: 5, timeUnit: nil, progressionUnit: nil, baseWeight: 55)
        let routineExercise10 = RoutineStrengthExercise(exercise: exercise10, sets: [Set(repsOrTime: 5, weight: 45), Set(repsOrTime: 5, weight: 45), Set(repsOrTime: 5, weight: 45)], progression: 5, timeUnit: nil, progressionUnit: nil, baseWeight: 45)
        
        let routine2 = Routine(name: "StrongLifts", days: [[routineExercise6, routineExercise7, routineExercise8], [routineExercise6, routineExercise9, routineExercise10]])
        routines.addRoutine(routine2)
        NSKeyedArchiver.archiveRootObject(routines, toFile: Routine.ArchiveURL.path)
    }
    
    func loadDefaultStrengthExercises() {
        // Strength Exercises
        strengthExercises.append(StrengthExercise(name: "Bicep Curl", text: "Grasp bar with shoulder width underhand grip. With elbows to side, raise bar until forearms are vertical. Lower until arms are fully extended. Repeat.", image: nil, muscleGroup: StrengthExercise.upperArms, weightType: StrengthExercise.dumbbell, isTimed: false))
        strengthExercises.append(StrengthExercise(name: "Triceps Dip", text: "Mount shoulder width dip bar, arms straight with shoulders above hands. Keep hips straight. Lower body until slight stretch is felt in shoulders. Push body up until arms are straight. Repeat.", image: nil, muscleGroup: StrengthExercise.upperArms, weightType: StrengthExercise.bodyWeight, isTimed: false))
        strengthExercises.append(StrengthExercise(name: "Reverse Curl", text: "Grasp bar with shoulder width overhand grip. With elbows to side, raise bar until forearms are vertical. Lower until arms are fully extended. Repeat.", image: nil, muscleGroup: StrengthExercise.forearms, weightType: StrengthExercise.dumbbell, isTimed: false))
        strengthExercises.append(StrengthExercise(name: "Neck Flexion", text: "Sit on bench facing away from middle pulley. Place neck in harness cable attachment. Place arms on lower thighs for support. Move head away from pulley by bending neck forward until chin touches upper chest. Return head by hyperextending neck and repeat.", image: nil, muscleGroup: StrengthExercise.neck, weightType: StrengthExercise.bodyWeight, isTimed: false))
        strengthExercises.append(StrengthExercise(name: "Overhead Press", text: "Grasp barbell from rack or clean barbell from floor with overhand grip, slightly wider than shoulder width. Position bar in front of neck. Press bar upward until arms are extended overhead. Lower to front of neck and repeat.", image: nil, muscleGroup: StrengthExercise.shoulders, weightType: StrengthExercise.barbell, isTimed: false))
        strengthExercises.append(StrengthExercise(name: "Shoulder Press", text: "Position dumbbells to each side of shoulders with elbows below wrists. Press dumbbells upward until arms are extended overhead. Lower to sides of shoulders and repeat.", image: nil, muscleGroup: StrengthExercise.shoulders, weightType: StrengthExercise.dumbbell, isTimed: false))
        strengthExercises.append(StrengthExercise(name: "Bent-over Row", text: "Bend knees slightly and bend over bar with back straight. Grasp bar with wide overhand grip. Pull bar to upper waist. Return until arms are extended and shoulders are stretched downward. Repeat.", image: nil, muscleGroup: StrengthExercise.back, weightType: StrengthExercise.barbell, isTimed: false))
        strengthExercises.append(StrengthExercise(name: "Pull-up", text: "Step up and grasp bar with overhand wide grip. Pull body up until chin is above bar. Lower body until arms and shoulders are fully extended. Repeat.", image: nil, muscleGroup: StrengthExercise.back, weightType: StrengthExercise.bodyWeight, isTimed: false))
        strengthExercises.append(StrengthExercise(name: "Bench Press", text: "Lie supine on bench. Dismount barbell from rack over upper chest using wide oblique overhand grip.", image: nil, muscleGroup: StrengthExercise.chest, weightType: StrengthExercise.barbell, isTimed: false))
        strengthExercises.append(StrengthExercise(name: "Chest Dip", text: "Mount wide dip bar with oblique grip (bar diagonal under palm), arms straight with shoulders above hands. Bend knees and hips slightly. Lower body by bending arms, allowing elbows to flare out to sides. When slight stretch is felt in chest or shoulders, push body up until arms are straight. Repeat.", image: nil, muscleGroup: StrengthExercise.chest, weightType: StrengthExercise.bodyWeight, isTimed: false))
        strengthExercises.append(StrengthExercise(name: "Plank", text: "Lie prone on mat. Place forearms on mat, elbows under shoulders. Place legs together with forefeet on floor. Raise body upward by straightening body in straight line. Hold position.", image: nil, muscleGroup: StrengthExercise.waist, weightType: StrengthExercise.bodyWeight, isTimed: true))
        strengthExercises.append(StrengthExercise(name: "Sit-up", text: "Hook feet under foot brace or secure low overhang. Lie supine on floor or bench with hips bent. Place hands behind neck or on side of neck. Raise torso from bench by bending waist and hips. Return until back of shoulders contact incline board. Repeat.", image: nil, muscleGroup: StrengthExercise.waist, weightType: StrengthExercise.bodyWeight, isTimed: true))
        strengthExercises.append(StrengthExercise(name: "Deadlift", text: "With feet flat beneath bar squat down and grasp bar with shoulder width or slightly wider overhand or mixed grip. Lift bar by extending hips and knees to full extension. Pull shoulders back at top of lift if rounded. Return weights to floor by bending hips back while allowing knees to bend forward, keeping back straight and knees pointed same direction as feet. Repeat.", image: nil, muscleGroup: StrengthExercise.hips, weightType: StrengthExercise.barbell, isTimed: false))
        strengthExercises.append(StrengthExercise(name: "Lunge", text: "Stand with dumbbells grasped to sides. Lunge forward with first leg. Land on heel then forefoot. Lower body by flexing knee and hip of front leg until knee of rear leg is almost in contact with floor. Return to original standing position by forcibly extending hip and knee of forward leg. Repeat by alternating lunge with opposite leg.", image: nil, muscleGroup: StrengthExercise.hips, weightType: StrengthExercise.dumbbell, isTimed: false))
        strengthExercises.append(StrengthExercise(name: "Squat", text: "From rack with barbell at upper chest height, position bar high on back of shoulders and grasp barbell to sides. Dismount bar from rack and stand with shoulder width stance. Squat down by bending hips back while allowing knees to bend forward, keeping back straight and knees pointed same direction as feet. Descend until thighs are just past parallel to floor. Extend knees and hips until legs are straight. Return and repeat.", image: nil, muscleGroup: StrengthExercise.thighs, weightType: StrengthExercise.barbell, isTimed: false))
        strengthExercises.append(StrengthExercise(name: "Inverse Leg Curl", text: "Adjust padded supports and kneel upright in apparatus. Position knees against large roller pad or near-upper side of padded hump, ankles between padded supports, and feet on platform. Hold weight to chest or behind neck. Lower body until horizontal by straightening knees. Raise body by flexing knees, only allowing hips to bend slightly. Repeat.", image: nil, muscleGroup: StrengthExercise.thighs, weightType: StrengthExercise.bodyWeight, isTimed: false))
        
        strengthExercises.append(StrengthExercise(name: "Standing Calf Raise", text: "Set barbell on power rack upper chest height with calf block under barbell. Position back of shoulders under barbell with both hands to sides. Position toes and balls of feet on calf block with arches and heels extending off. Lean barbell against rack and raise from supports by extending knees and hips. Support barbell against verticals with both hands to sides. Raise heels by extending ankles as high as possible. Lower heels by bending ankles until calves are stretched. Repeat.", image: nil, muscleGroup: StrengthExercise.calves, weightType: StrengthExercise.barbell, isTimed: false))
        NSKeyedArchiver.archiveRootObject(strengthExercises, toFile: StrengthExercise.ArchiveURL.path)
        
        // Cardio Exercises
        cardioExercises.append(CardioExercise(name: "Running", text: "Vroom vroom!", image: nil))
        NSKeyedArchiver.archiveRootObject(cardioExercises, toFile: CardioExercise.ArchiveURL.path)
    }
    
    func loadRoutines() -> Routines? {
        return NSKeyedUnarchiver.unarchiveObject(withFile: Routines.ArchiveURL.path) as? Routines
    }
    
    func loadCardioExercises() -> [CardioExercise]? {
        return NSKeyedUnarchiver.unarchiveObject(withFile: CardioExercise.ArchiveURL.path) as? [CardioExercise]
    }
    
    func loadStrengthExercises() -> [StrengthExercise]? {
        return NSKeyedUnarchiver.unarchiveObject(withFile: StrengthExercise.ArchiveURL.path) as? [StrengthExercise]
    }
    
    func loadCurrentWorkout() -> CurrentWorkout? {
        return NSKeyedUnarchiver.unarchiveObject(withFile: CurrentWorkout.ArchiveURL.path) as? CurrentWorkout
    }
    
    // MARK: - PickerView
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if component == 0 {
            return routines.count()
        }
        else {
            return routines.routines[pickerView.selectedRow(inComponent: 0)].days.count
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        //color the current workout
        if component == 0 {
            return routines.routines[row].name
        }
        else {
            return "Day \(row + 1)"
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if component == 0 {
            pickerView.reloadComponent(1)
        }
        //sets the button title accordingly
        getStartedButton.setTitle(currentWorkout.isOngoing && routines.routines[pickerView.selectedRow(inComponent: 0)].name == currentWorkout.routine.name && pickerView.selectedRow(inComponent: 1) == currentWorkout.day ? "Continue Workout": "Start Workout", for: UIControlState())
    }

    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 2
    }
    
    func pickerView(_ pickerView: UIPickerView, widthForComponent component: Int) -> CGFloat {
        if component == 1 {
            return pickerView.frame.width * 0.20
        }
        else {
            return pickerView.frame.width * 0.60
        }
    }

    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        let label: UILabel
        //if it's not on screen
        if view == nil {
            label = UILabel()
        }
        //if it's already on screen
        else {
            label = view as! UILabel
        }
        //change the color of the selector bars
        pickerView.subviews[1].backgroundColor = Color.greenColor
        pickerView.subviews[2].backgroundColor = Color.greenColor
        
        //color the current workout
        if component == 0 && routines.routines[row].name == currentWorkout.routine.name {
            label.textColor = Color.greenColor
        }
        else if component == 1 && routines.routines[pickerView.selectedRow(inComponent: 0)].name == currentWorkout.routine.name && row == currentWorkout.day {
            label.textColor = Color.greenColor
        }
        
        label.textAlignment = NSTextAlignment.center
        label.font = UIFont.systemFont(ofSize: UIFont.systemFontSize + 5)
        if component == 0 {
            label.text = routines.routines[row].name
        }
        else {
            label.text = "Day \(row + 1)"
        }
        label.font = UIFont(name: "HelveticaNeue", size: 22)
        
        return label
    }
    

    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "StartOrContinueRoutine" {
            let destination = segue.destination as! ActiveRoutineViewController
            //check if current workout is the one that was loaded, else set the new one
            
            if currentWorkout.routine.name == routines.routines[routinePicker.selectedRow(inComponent: 0)].name && routinePicker.selectedRow(inComponent: 1) == currentWorkout.day {               destination.currentWorkout = currentWorkout
            }
            else {
                destination.currentWorkout = CurrentWorkout(routine: routines.routines[routinePicker.selectedRow(inComponent: 0)], day: routinePicker.selectedRow(inComponent: 1))
            }
        }
    }

}

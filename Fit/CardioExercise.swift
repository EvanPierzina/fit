//
//  CardioExercise.swift
//  Fit
//
//  Created by Evan Pierzina on 3/5/16.
//  Copyright © 2016 Evan Pierzina. All rights reserved.
//

import UIKit

//needs @objc otherwise a runtime error will occur when calling Array<Array<Exercise>>.append(Array<Exercise>)
//"swift array cannot be bridged from objective-c"

@objc class CardioExercise : NSObject, NSCoding, Exercise {
    
    var name: String
    var text: String?
    var image: UIImage?
    
    // Initializer
    init(name: String, text: String?, image: UIImage?) {
        self.name = name
        self.text = text
        self.image = image
        
        super.init()
    }
    
    // Archive Paths
    static let DocumentsDirectory = FileManager().urls(for: .documentDirectory, in: .userDomainMask).first!
    static let ArchiveURL = DocumentsDirectory.appendingPathComponent("CardioExercises")
    
    // NSCoding
    func encode(with aCoder: NSCoder) {
        aCoder.encode(name, forKey: "name")
        aCoder.encode(text, forKey: "text")
        aCoder.encode(image, forKey: "image")
    }
    
    required convenience init?(coder aDecoder: NSCoder) {
        let name = aDecoder.decodeObject(forKey: "name") as! String
        let text = aDecoder.decodeObject(forKey: "text") as? String
        let image = aDecoder.decodeObject(forKey: "image") as? UIImage
        
        self.init(name: name, text: text, image: image)
    }
}

//
//  StrengthExercise.swift
//  Fit
//
//  Created by Evan Pierzina on 3/5/16.
//  Copyright © 2016 Evan Pierzina. All rights reserved.
//

import UIKit

//needs @objc otherwise a runtime error will occur when calling Array<Array<Exercise>>.append(Array<Exercise>)
//"swift array cannot be bridged from objective-c"

@objc class StrengthExercise : NSObject, NSCoding, Exercise {
    
    var name: String
    var text: String?
    var image: UIImage?
    var muscleGroup : Int   //arms, back, etc
    var weightType: Int     //barbell, dumbell, etc
    var isTimed: Bool       //reps or timed
    
    // Muscle Group constants
    // arms
    static let upperArms = 0
    static let forearms = 1
    // upper body
    static let neck = 2
    static let shoulders = 3
    static let back = 4
    static let chest = 5
    // lower body
    static let waist = 6
    static let hips = 7
    static let thighs = 8
    static let calves = 9
    
    // Weight Type constants
    static let barbell = 0
    static let dumbbell = 1
    static let kettlebell = 2
    static let cable = 3
    static let bodyWeight = 4
    
    // Initializer
    init(name: String, text: String?, image: UIImage?, muscleGroup: Int, weightType: Int, isTimed: Bool) {
        self.name = name
        self.text = text
        self.image = image
        self.muscleGroup = muscleGroup
        self.weightType = weightType
        self.isTimed = isTimed
        
        super.init()
    }
    
    // Archive Paths
    static let DocumentsDirectory = FileManager().urls(for: .documentDirectory, in: .userDomainMask).first!
    static let ArchiveURL = DocumentsDirectory.appendingPathComponent("StrengthExercises")
    
    // NSCoding
    func encode(with aCoder: NSCoder) {
        aCoder.encode(name, forKey: "name")
        aCoder.encode(text, forKey: "text")
        aCoder.encode(image, forKey: "image")
        aCoder.encode(muscleGroup, forKey: "muscleGroup")
        aCoder.encode(weightType, forKey: "weightType")
        aCoder.encode(isTimed, forKey: "isTimed")
    }
    
    required convenience init?(coder aDecoder: NSCoder) {
        let name = aDecoder.decodeObject(forKey: "name") as! String
        let text = aDecoder.decodeObject(forKey: "text") as? String
        let image = aDecoder.decodeObject(forKey: "image") as? UIImage
        let muscleGroup = aDecoder.decodeInteger(forKey: "muscleGroup")
        let weightType = aDecoder.decodeInteger(forKey: "weightType")
        let isTimed = aDecoder.decodeBool(forKey: "isTimed")
    
        self.init(name: name, text: text, image: image, muscleGroup: muscleGroup, weightType: weightType, isTimed: isTimed)
    }
}

//
//  Color.swift
//  Fit
//
//  Created by Evan Pierzina on 4/16/16.
//  Copyright © 2016 Evan Pierzina. All rights reserved.
//

import UIKit

class Color {
    static var greenColor = UIColor(colorLiteralRed: 161.0/255, green: 198.0/255, blue: 82.0/255, alpha: 1)
    static var viewBackgroundColor = UIColor(colorLiteralRed: 234.0/255, green: 234.0/255, blue: 234.0/255, alpha: 1)
    static var contentBackgroundColor = UIColor(colorLiteralRed: 248.0/255, green: 248.0/255, blue: 248.0/255, alpha: 1)
    
    static func defaultColors() {
        viewBackgroundColor = UIColor(colorLiteralRed: 234.0/255, green: 234.0/255, blue: 234.0/255, alpha: 1)
        contentBackgroundColor = UIColor(colorLiteralRed: 248.0/255, green: 248.0/255, blue: 248.0/255, alpha: 1)
    }
    
    static func darkColors() {
        viewBackgroundColor = UIColor(colorLiteralRed: 25.0/255, green: 25.0/255, blue: 25.0/255, alpha: 1)
        contentBackgroundColor = UIColor(colorLiteralRed: 40.0/255, green: 40.0/255, blue: 40.0/255, alpha: 1)
    }
}
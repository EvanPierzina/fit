//
//  CurrentWorkout.swift
//  Fit
//
//  Created by Evan Pierzina on 4/16/16.
//  Copyright © 2016 Evan Pierzina. All rights reserved.
//

import UIKit

class CurrentWorkout : NSObject, NSCoding {
    
    var routine: Routine
    var day: Int
    var isOngoing: Bool = false
    var activeButtons: Int = 0
    
    init(routine: Routine, day: Int) {
        self.routine = routine
        self.day = day
        
        super.init()
    }
    
    init(routine: Routine, day: Int, isOngoing: Bool, activeButtons: Int) {
        self.routine = routine
        self.day = day
        self.isOngoing = isOngoing
        
        super.init()
    }
    
    //Archive Paths
    static let DocumentsDirectory = FileManager().urls(for: .documentDirectory, in: .userDomainMask).first!
    static let ArchiveURL = DocumentsDirectory.appendingPathComponent("CurrentWorkout")
    
    //NSCoding
    func encode(with aCoder: NSCoder) {
        aCoder.encode(routine, forKey: "routine")
        aCoder.encode(day, forKey: "day")
        aCoder.encode(isOngoing, forKey: "isOngoing")
        aCoder.encode(activeButtons, forKey: "activeButtons")
    }
    
    required convenience init?(coder aDecoder: NSCoder) {
        let routine = aDecoder.decodeObject(forKey: "routine") as! Routine
        let day = aDecoder.decodeInteger(forKey: "day")
        let isOngoing = aDecoder.decodeBool(forKey: "isOngoing")
        let activeButtons = aDecoder.decodeInteger(forKey: "activeButtons")
        
        self.init(routine: routine, day: day, isOngoing: isOngoing, activeButtons: activeButtons)
    }
}

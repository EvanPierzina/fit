//
//  ExerciseViewController.swift
//  Fit
//
//  Created by Evan Pierzina on 3/6/16.
//  Copyright © 2016 Evan Pierzina. All rights reserved.
//

import UIKit

class ExerciseViewController: UIViewController, UITextViewDelegate, UITextFieldDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    @IBOutlet weak var photoLabel: UILabel!
    @IBOutlet weak var deleteButton: UIButton!
    @IBOutlet weak var infoLabel: UILabel!
    @IBOutlet weak var instructionText: UITextView!
    @IBOutlet weak var textField: UITextField!
    @IBOutlet weak var saveButton: UIBarButtonItem!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var imageView: UIImageView!
    
    @IBOutlet weak var exerciseTypeControl: UISegmentedControl!
    @IBOutlet weak var strengthExerciseView: UIView!
    @IBOutlet weak var generalMuscleGroupControl: UISegmentedControl! //0 = arms, 1 = upper, 2 = lower
    @IBOutlet weak var armsGroupControl: UISegmentedControl!
    @IBOutlet weak var upperBodyGroupControl: UISegmentedControl!
    @IBOutlet weak var lowerBodyGroupControl: UISegmentedControl!
    @IBOutlet weak var methodControl: UISegmentedControl!
    @IBOutlet weak var weightTypeControl: UISegmentedControl!
    
    //since this view may show modally, it must keep the UI theme consistent
    //via passing infomation through the segue
    var navBarColor : UIColor?
    var barItemColor : UIColor?
    var titleAttributes : [String : AnyObject]?
    
    var strengthExercise : StrengthExercise? // Either passed through the segue or will be used to create a new exercise
    var cardioExercise : CardioExercise?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if navBarColor != nil {
            navigationController!.navigationBar.barTintColor = navBarColor
            navigationController!.navigationBar.tintColor = barItemColor
            navigationController!.navigationBar.titleTextAttributes = titleAttributes
        }
        
        // If an exercise was passed, show the stored options
        if strengthExercise != nil {
            strengthExerciseView.isHidden = false
            
            switch (strengthExercise!.muscleGroup) {
            //neck
            case StrengthExercise.neck:
                generalMuscleGroupControl.selectedSegmentIndex = 1 //upper body
                armsGroupControl.isHidden = true
                upperBodyGroupControl.isHidden = false
                lowerBodyGroupControl.isHidden = true
                upperBodyGroupControl.selectedSegmentIndex = 0
                break
            //shoulders
            case StrengthExercise.shoulders:
                generalMuscleGroupControl.selectedSegmentIndex = 1 //upper body
                armsGroupControl.isHidden = true
                upperBodyGroupControl.isHidden = false
                lowerBodyGroupControl.isHidden = true
                upperBodyGroupControl.selectedSegmentIndex = 1
                break
            //upperArms
            case StrengthExercise.upperArms:
                generalMuscleGroupControl.selectedSegmentIndex = 0 //arms
                armsGroupControl.isHidden = false
                upperBodyGroupControl.isHidden = true
                lowerBodyGroupControl.isHidden = true
                upperBodyGroupControl.selectedSegmentIndex = 0
                break
            //forearms
            case StrengthExercise.forearms:
                generalMuscleGroupControl.selectedSegmentIndex = 0 //arms
                armsGroupControl.isHidden = false
                upperBodyGroupControl.isHidden = true
                lowerBodyGroupControl.isHidden = true
                upperBodyGroupControl.selectedSegmentIndex = 1
                break
            //back
            case StrengthExercise.back:
                generalMuscleGroupControl.selectedSegmentIndex = 1 //upper body
                armsGroupControl.isHidden = true
                upperBodyGroupControl.isHidden = false
                lowerBodyGroupControl.isHidden = true
                upperBodyGroupControl.selectedSegmentIndex = 2
                break
            //chest
            case StrengthExercise.chest:
                generalMuscleGroupControl.selectedSegmentIndex = 1 //upper body
                armsGroupControl.isHidden = true
                upperBodyGroupControl.isHidden = false
                lowerBodyGroupControl.isHidden = true
                upperBodyGroupControl.selectedSegmentIndex = 3
                break
            //waist
            case StrengthExercise.waist:
                generalMuscleGroupControl.selectedSegmentIndex = 2 //lower body
                armsGroupControl.isHidden = true
                upperBodyGroupControl.isHidden = true
                lowerBodyGroupControl.isHidden = false
                upperBodyGroupControl.selectedSegmentIndex = 0
                break
            //hips
            case StrengthExercise.hips:
                generalMuscleGroupControl.selectedSegmentIndex = 2 //lower body
                armsGroupControl.isHidden = true
                upperBodyGroupControl.isHidden = true
                lowerBodyGroupControl.isHidden = false
                upperBodyGroupControl.selectedSegmentIndex = 1
                break
            //thighs
            case StrengthExercise.thighs:
                generalMuscleGroupControl.selectedSegmentIndex = 2 //lower body
                armsGroupControl.isHidden = true
                upperBodyGroupControl.isHidden = true
                lowerBodyGroupControl.isHidden = false
                upperBodyGroupControl.selectedSegmentIndex = 2
                break
            //calves
            case StrengthExercise.calves:
                generalMuscleGroupControl.selectedSegmentIndex = 2 //lower body
                armsGroupControl.isHidden = true
                upperBodyGroupControl.isHidden = true
                lowerBodyGroupControl.isHidden = false
                upperBodyGroupControl.selectedSegmentIndex = 3
                break
            default:
                break
            }
            
            instructionText.text = strengthExercise?.text
            infoLabel.isHidden = instructionText.text != ""
            photoLabel.isHidden = strengthExercise?.image != nil
            methodControl.selectedSegmentIndex = strengthExercise!.isTimed ? 1 : 0
            weightTypeControl.selectedSegmentIndex = strengthExercise!.weightType
            textField.text = strengthExercise!.name
            imageView.image = strengthExercise?.image
            self.title = strengthExercise!.name
            deleteButton.isHidden = false
        }
        else if cardioExercise != nil {
            strengthExerciseView.isHidden = true
            textField.text = cardioExercise!.name
            exerciseTypeControl.selectedSegmentIndex = 1
            deleteButton.isHidden = false
        }
        
        saveButton.isEnabled = textField.text != ""
    }
    
    override func viewDidAppear(_ animated: Bool) {
        //set up event to dismiss keyboard on a tap outside of a text field
        //default UITapGestureRecognizer = 1 tap with 1 finger
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(ExerciseViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)
        
        //set up observers for keyboard events
        NotificationCenter.default.addObserver(self, selector: #selector(ExerciseViewController.adjustViewForKeyboard(_:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(ExerciseViewController.adjustViewForKeyboard(_:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    // MARK: - Segmented Control
    
    @IBAction func SegmentValueChanged(_ sender: UISegmentedControl) {
        if sender === exerciseTypeControl {
            if exerciseTypeControl.selectedSegmentIndex == 0 {
                strengthExerciseView.isHidden = false
            }
            else {
                strengthExerciseView.isHidden = true
            }
        }
        else if sender === generalMuscleGroupControl {
            switch (generalMuscleGroupControl.selectedSegmentIndex) {
            case 0:
                armsGroupControl.isHidden = false
                upperBodyGroupControl.isHidden = true
                lowerBodyGroupControl.isHidden = true
                break
            case 1:
                armsGroupControl.isHidden = true
                upperBodyGroupControl.isHidden = false
                lowerBodyGroupControl.isHidden = true
                break
            default:
                armsGroupControl.isHidden = true
                upperBodyGroupControl.isHidden = true
                lowerBodyGroupControl.isHidden = false
            }
        }
    }
    
    
    // MARK: - Actions
    
    @IBAction func addImage(_ sender: UITapGestureRecognizer) {
        // UIImagePickerController is a view controller that lets a user pick media from their photo library.
        let imagePickerController = UIImagePickerController()
        
        // Only allow photos to be picked, not taken.
        imagePickerController.sourceType = .photoLibrary
        
        // Make sure ViewController is notified when the user picks an image.
        imagePickerController.delegate = self
        
        present(imagePickerController, animated: true, completion: nil)
    }
    
    // MARK: - Text Input
    
    @IBAction func nameFieldChanged(_ sender: UITextField) {
        saveButton.isEnabled = textField.text != ""
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        infoLabel.isHidden = true
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        infoLabel.isHidden = textView.text != ""
        textView.resignFirstResponder()
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        dismissKeyboard()
        return true
    }
    
    func dismissKeyboard() {
        //resigns first responder status for the UIView (aka hides the keyboard)
        view.endEditing(true)
    }
    
    func adjustViewForKeyboard(_ notification: Notification) {
        //grab the notification dictionary
        let userInfo:NSDictionary = notification.userInfo! as NSDictionary
        //get the size of the keyboard being loaded
        let keyboardScreenEndFrame:CGRect = (userInfo[UIKeyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
        //account for view rotations
        let keyboardViewEndFrame:CGRect = view.convert(keyboardScreenEndFrame, from: view.window)
        //determine if the keyboard is appearing or disappearing and adjust the view
        if notification.name == NSNotification.Name.UIKeyboardWillHide {
            //reset scrolling area
            scrollView.contentInset = UIEdgeInsets.zero
        }
        else {
            //calculate scrolling area
            scrollView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: keyboardViewEndFrame.height, right: 0)
        }
        //update scroll bar range
        scrollView.scrollIndicatorInsets = scrollView.contentInset
    }
    
    // MARK: Navigation
    
    @IBAction func cancel(_ sender: AnyObject) {
        // Depending on style of presentation (modal or push presentation), this view controller needs to be dismissed in two different ways.
        let isPresentingInAddMode = presentingViewController is UINavigationController
        
        if isPresentingInAddMode {
            dismiss(animated: true, completion: nil)
        } else {
            navigationController!.popViewController(animated: true)
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if saveButton === sender as AnyObject? {
            //build the exercise and store it so the next view controller can grab it
            let name = textField.text ?? "" // Uses "" if nil
            let text = instructionText.text
            let image = imageView.image
            
            if exerciseTypeControl.selectedSegmentIndex == 0 {
                //strength exercise
                var muscleGroup: Int
                switch (generalMuscleGroupControl.selectedSegmentIndex) {
                //arms
                case 0:
                    muscleGroup = StrengthExercise.upperArms + armsGroupControl.selectedSegmentIndex
                    break
                //upper body
                case 1:
                    muscleGroup = StrengthExercise.neck + upperBodyGroupControl.selectedSegmentIndex
                    break
                //lower body
                default:
                    muscleGroup = StrengthExercise.waist + lowerBodyGroupControl.selectedSegmentIndex
                    break
                }
                let weightType = weightTypeControl.selectedSegmentIndex
                let isTimed = methodControl.selectedSegmentIndex != 0
                
                strengthExercise = StrengthExercise(name: name, text: text, image: image, muscleGroup: muscleGroup, weightType: weightType, isTimed: isTimed)
                cardioExercise = nil //in case the type changed
            }
            else {
                cardioExercise = CardioExercise(name: name, text: text, image: image)
                strengthExercise = nil
            }
        }
    }
    
    // MARK: UIImagePickerControllerDelegate
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        // Dismiss the picker if the user canceled.
        dismiss(animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        // The info dictionary contains multiple representations of the image, and this uses the original.
        let selectedImage = info[UIImagePickerControllerOriginalImage] as! UIImage
        
        // Set photoImageView to display the selected image.
        imageView.image = selectedImage
        photoLabel.isHidden = true
        
        // Dismiss the picker.
        dismiss(animated: true, completion: nil)
    }
}

//
//  SettingsViewController.swift
//  Fit
//
//  Created by Evan Pierzina on 3/4/16.
//  Copyright © 2016 Evan Pierzina. All rights reserved.
//

import UIKit

class SettingsViewController: UIViewController {

    override func viewWillAppear(_ animated: Bool) {
        //set the title (it's a bit more complicated using this view hierarchy)
        self.tabBarController?.navigationItem.title = "Settings"
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

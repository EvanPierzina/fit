//
//  Exercise.swift
//  Fit
//
//  Created by Evan Pierzina on 3/5/16.
//  Copyright © 2016 Evan Pierzina. All rights reserved.
//

import UIKit

//needs @objc otherwise an array of this type can't be converted to AnyObject

@objc protocol Exercise {
    var name: String { get set }
    var text: String? { get set }
    var image: UIImage? { get set }
}




//@objc class Exercise : NSObject, NSCoding {
//    
//    var name: String
//    var isTimed: Bool //reps or timed
//    var image: UIImage?
//    var instructions : String?
//    var usesBarbell: Bool
//    var muscleGroup: Int
//    
//    // Muscle Group constants
//    static let arms = 0
//    static let back = 1
//    static let chest = 2
//    static let legs = 3
//    static let waist = 4
//    static let other = 5
//    
//    init(name: String, isTimed: Bool, image: UIImage?, instructions: String?, usesBarbell: Bool, muscleGroup: Int) {
//    self.name = name
//    self.isTimed = isTimed
//    self.image = image
//    self.instructions = instructions
//    self.usesBarbell = usesBarbell
//    self.muscleGroup = muscleGroup
//    
//    super.init()
//    }
//    
//    //Archive Paths
//    static let DocumentsDirectory = NSFileManager().URLsForDirectory(.DocumentDirectory, inDomains: .UserDomainMask).first!
//    static let ArchiveURL = DocumentsDirectory.URLByAppendingPathComponent("Exercises")
//    
//    //NSCoding
//    
//    func encodeWithCoder(aCoder: NSCoder) {
//        aCoder.encodeObject(name, forKey: "name")
//        aCoder.encodeBool(isTimed, forKey: "isTimed")
//        aCoder.encodeObject(image, forKey: "image")
//        aCoder.encodeObject(instructions, forKey: "instructions")
//        aCoder.encodeBool(usesBarbell, forKey: "usesBarbell")
//        aCoder.encodeInteger(muscleGroup, forKey: "muscleGroup")
//    }
//    
//    required convenience init?(coder aDecoder: NSCoder) {
//        let name = aDecoder.decodeObjectForKey("name") as! String
//        let isTimed = aDecoder.decodeBoolForKey("isTimed")
//        let image = aDecoder.decodeObjectForKey("image") as? UIImage
//        let instructions = aDecoder.decodeObjectForKey("instructions") as? String
//        let usesBarbell = aDecoder.decodeBoolForKey("usesBarbell")
//        let muscleGroup = aDecoder.decodeIntegerForKey("muscleGroup")
//    
//        self.init(name: name, isTimed: isTimed, image: image, instructions: instructions, usesBarbell: usesBarbell, muscleGroup: muscleGroup)
//    }
//}
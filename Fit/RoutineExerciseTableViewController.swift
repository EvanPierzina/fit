//
//  RoutineExerciseTableViewController.swift
//  Fit
//
//  Created by Evan Pierzina on 3/12/16.
//  Copyright © 2016 Evan Pierzina. All rights reserved.
//

import UIKit

class RoutineExerciseTableViewController: UITableViewController {
    
    var strengthExercises : [StrengthExercise]? //grouped by muscleGroup
    var cardioExercises : [CardioExercise]?
    var selectedExercise : Exercise?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        strengthExercises = loadStrengthExercises()
        cardioExercises = loadCardioExercises()
    }
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return  section == 0 ? strengthExercises!.count : cardioExercises!.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "FitImageCell", for: indexPath)
        
        if indexPath.section == 0 && strengthExercises != nil {
            cell.textLabel?.text = strengthExercises![indexPath.row].name
            
            switch strengthExercises![indexPath.row].muscleGroup {
            case StrengthExercise.upperArms:
                cell.imageView?.image = UIImage(named: "strength1")
                break
            case StrengthExercise.forearms:
                cell.imageView?.image = UIImage(named: "strength2")
                break
            case StrengthExercise.neck:
                cell.imageView?.image = UIImage(named: "strength3")
                break
            case StrengthExercise.shoulders:
                cell.imageView?.image = UIImage(named: "strength4")
                break
            case StrengthExercise.back:
                cell.imageView?.image = UIImage(named: "strength5")
                break
            case StrengthExercise.chest:
                cell.imageView?.image = UIImage(named: "strength6")
                break
            case StrengthExercise.waist:
                cell.imageView?.image = UIImage(named: "strength7")
                break
            case StrengthExercise.hips:
                cell.imageView?.image = UIImage(named: "strength8")
                break
            case StrengthExercise.thighs:
                cell.imageView?.image = UIImage(named: "strength9")
                break
            case StrengthExercise.calves:
                cell.imageView?.image = UIImage(named: "strength10")
                break
            default:
                break
            }
        }
        else if cardioExercises != nil {
            cell.textLabel?.text = cardioExercises![indexPath.row].name
            cell.imageView?.image = UIImage(named: "cardio13")
            
        }
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return section == 0 ? "Strength" : "Cardio"
    }
    
    // MARK: - Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        //set the selected exercise
        let selectedIndexPath = tableView.indexPathForSelectedRow!
        if selectedIndexPath.section == 0 {
            selectedExercise = strengthExercises![selectedIndexPath.row]
        }
        else {
            selectedExercise = cardioExercises![selectedIndexPath.row]
        }
    }
    
    @IBAction func cancel(_ sender: AnyObject) {
        navigationController!.popViewController(animated: true)
    }
    
    // MARK: - NSCoding
    
    func loadStrengthExercises() -> [StrengthExercise]? {
        return NSKeyedUnarchiver.unarchiveObject(withFile: StrengthExercise.ArchiveURL.path) as? [StrengthExercise]
    }
    
    func loadCardioExercises() -> [CardioExercise]? {
        return NSKeyedUnarchiver.unarchiveObject(withFile: CardioExercise.ArchiveURL.path) as? [CardioExercise]
    }
}
